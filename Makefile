LIB_DIR = /opt/DispLib
SRC_DIR = ./srcDisp
PY=/opt/anaconda3/bin/python3

default: cyMP
	
cython_wrap: $(SRC_DIR)/cython/setup.py $(SRC_DIR)/cython/DispCython.pyx 
	$(PY) $(SRC_DIR)/cython/setup.py build_ext --inplace 
	rm -f DispCython.c 
	rm -Rf build
	mv ./DispCython*.so $(LIB_DIR)/lib/

cyCUDA: sharedCUDA $(SRC_DIR)/cython/setup.py $(SRC_DIR)/cython/DispCython.pyx 
	$(PY) $(SRC_DIR)/cython/setup.py build_ext --inplace 
	rm -f DispCython.c 
	rm -Rf build
	mv ./DispCython*.so $(LIB_DIR)/lib/
	
cyMP: sharedMP  $(SRC_DIR)/cython/setup.py $(SRC_DIR)/cython/DispCython.pyx 
	$(PY) $(SRC_DIR)/cython/setup.py build_ext --inplace 
	rm -f DispCython.c 
	rm -Rf build
	mv ./DispCython*.so $(LIB_DIR)/lib/

sharedMP:
	mkdir -p $(LIB_DIR)/lib
	make -C $(SRC_DIR) sharedMP path=$(LIB_DIR)
	
sharedCUDA:
	mkdir -p $(LIB_DIR)/lib
	make -C $(SRC_DIR) sharedCUDA path=$(LIB_DIR)
	

clean:
	rm $(LIB_DIR)/lib/*.so & rm $(SRC_DIR)/cython/DispCython.c & make -C $(SRC_DIR) clean
	
