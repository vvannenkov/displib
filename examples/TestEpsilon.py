#calculate epsilon in one point

import sys
sys.path.append('/opt/DispLib/lib/')
#export LD_LIBRARY_PATH=/opt/DispLib/lib$LD_LIBRARY_PATH

import time
import numpy as np

from DispCython import pyColdEps
from DispCython import pyPrintEps
from DispCython import pyVacEps
from DispCython import pyEps
from DispCython import PrintAllCUDAdevices
from DispCython import PrintCUDAdeviceProp


from Params import *
#PlasmaParamsCold -- Cold plasma
#PlasmaParamsMaxw -- Maxwellian plasma
#IntParamsPlasma -- Integration parametrs of  Maxwellian plasma

#beam
#BeamParamsCold -- Cold beam
#BeamParamsHotMaxw -- Maxwellian distribution
#IntParamsBeamMaxw -- Integration parameters 
#BeamParamsHotRB -- Ring-beam distribution
#IntParamsBeamRB -- Integration parameters 

#Calculate on GPU (tr_use_GPU=1) or CPU  (tr_use_GPU!=1)
tr_use_GPU=0

if tr_use_GPU!=1:
#use all available CPU threads
	PrintAllCUDAdevices()

else:
#use GPU number 0
	PrintAllCUDAdevices()
	print('Device number',IntParamsPlasma[7],'will be used')
	PrintCUDAdeviceProp(IntParamsPlasma[7])

	
kl=1.15
kt=0.05
w_real=0.98		
w_imag=0.02

print("Vacuum")
pyPrintEps(pyVacEps(),4)#Vacuum eps.

start = time.time()
print("Maxwellian beam")
Eps=pyEps(w_real,w_imag,kl, kt,BeamParamsHotMaxw,IntParamsBeamMaxw)
pyPrintEps(Eps,4)

print("Ring-beam distribution")
Eps=pyEps(w_real,w_imag,kl, kt,BeamParamsHotRB,IntParamsBeamRB)
pyPrintEps(Eps,4)

print("Maxwellian plasma")
Eps=pyEps(w_real,w_imag,kl, kt,PlasmaParamsMaxw,IntParamsPlasma)
pyPrintEps(Eps,4)

print("Cold plasma")
Eps=pyEps(w_real,w_imag,kl, kt,PlasmaParamsCold,[])
pyPrintEps(Eps,4)
end = time.time()
Time=end - start

print('3 hot + 1 cold Epsilons calc time=',Time)



