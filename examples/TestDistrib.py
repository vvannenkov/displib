#plot particles distributions
import sys
sys.path.append('/opt/DispLib/lib/')
#export LD_LIBRARY_PATH=/opt/DispLib/lib$LD_LIBRARY_PATH

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import rc
from matplotlib import cm

from math import pi,sqrt

gs =gridspec.GridSpec(1,3)
def SubPlot(x,y,fig):
	return fig.add_subplot(gs[y,x])


from DispCython import pyDistribution
from Params import *
#PlasmaParamsCold -- Cold plasma
#PlasmaParamsMaxw -- Maxwellian plasma
#IntParamsPlasma -- Integration parametrs of  Maxwellian plasma

#beam
#BeamParamsCold -- Cold beam
#BeamParamsHotMaxw -- Maxwellian distribution
#IntParamsBeamMaxw -- Integration parameters 
#BeamParamsHotRB -- Ring-beam distribution
#IntParamsBeamRB -- Integration parameters 



def PlotDistrib(SortParams,SortIntParams,title):
	fig = plt.figure(figsize=(15, 5))

	fig.suptitle(title, fontsize=16)
	Min_pl=SortIntParams[0]
	Max_pl=SortIntParams[1]
	Nl=SortIntParams[2]
	Min_pt=SortIntParams[3]
	Max_pt=SortIntParams[4]
	Nt=SortIntParams[5]

	PL=np.linspace(Min_pl, Max_pl, Nl)
	PT=np.linspace(Min_pt, Max_pt, Nt)

	Distrib=np.empty((Nl,Nt))
	DlDistrib=np.empty((Nl,Nt))
	DtDistrib=np.empty((Nl,Nt))
	il=0
	it=0
	Integral=0
	dpl=PL[1]-PL[0]
	dpt=PT[1]-PT[0]
	for pl in PL:
		it=0
		for pt in PT:
			Distrib[il][it],DlDistrib[il][it],DtDistrib[il][it]=pyDistribution(SortParams,SortParams[6],pl,pt)
			it=it+1
		il=il+1

	axes=[]
	axes.append(SubPlot(0,0,fig))
	axes.append(SubPlot(1,0,fig))
	axes.append(SubPlot(2,0,fig))


	axes[0].imshow(Distrib.T,origin='lower',aspect='auto',extent=[Min_pl,Max_pl,Min_pt,Max_pt],interpolation='kaiser')
	axes[0].set_title("$f(p_\parallel,p_\perp)$")
	axes[1].imshow(DlDistrib.T,origin='lower',aspect='auto',extent=[Min_pl,Max_pl,Min_pt,Max_pt],interpolation='kaiser')
	axes[1].set_title("$\partial f(p_\parallel,p_\perp)/\partial p_\parallel$")
	axes[2].imshow(DtDistrib.T,origin='lower',aspect='auto',extent=[Min_pl,Max_pl,Min_pt,Max_pt],interpolation='kaiser')
	axes[2].set_title("$\partial f(p_\parallel,p_\perp)/\partial p_\perp$")

	for ax in axes:
		ax.set_xlabel('$p_\parallel/m_ec$')
		ax.set_ylabel('$p_\perp/m_ec$')


PlotDistrib(BeamParamsHotRB,IntParamsBeamRB,'Beam with pitch-angle distribution in magnetic mirror')
PlotDistrib(BeamParamsHotMaxw,IntParamsBeamMaxw,'Beam with Maxwellian distribution')
PlotDistrib(PlasmaParamsMaxw,IntParamsPlasma,'Plasma with Maxwellian distribution')
plt.tight_layout()
plt.show()
