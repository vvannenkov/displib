from math import pi,sqrt
from DispCython import pyDistribution_Norm
from DispCython import pyDistribution_Numerical_Norm
from DispCython import pyMake_Distribution_Norm

#number of GPU to use (for CUDA version) or number of CPU threads (for OpenMP version) (0 mean "all")
DevProp=0

#Electron beam
Omega=0.3#cyclotron freq.
vb=0.95#beam velocity
nb=0.005#relative density
me=1#mass 
Ze=1#charge (in units of -e)
pP=vb/sqrt(1-vb*vb)#directed momentum
theta=0#angle between vb and magnetic field
#beam temperature Maxwellian distrib
Te_t=10e+3#eV transverse
dPtB=sqrt(2.*Te_t/511000) 
Te_l=1e+3#eV longitude
dPlB=sqrt(2.*Te_l/511000)  
#integration limits
Min_pl=pP-5*dPlB
Max_pl=pP+5*dPlB
Min_pt=0
Max_pt=5*dPtB
#number of segments 
Nl=100#longitude
Nt=100#transverse
Ng=200#G-integrals


BeamParamsCold=[0,nb,me,Ze,Omega,vb]#Cold approximation

Distr_type=1# Maxwellian distribution
BeamParamsHotMaxw=[Distr_type,nb,me,Ze,Omega,vb,1,theta,dPlB,dPtB]#Maxwellian distribution
IntParamsBeamMaxw=[Min_pl,Max_pl,Nl,Min_pt,Max_pt,Nt,Ng,DevProp]#Integration parameters 
pyMake_Distribution_Norm(BeamParamsHotMaxw,IntParamsBeamMaxw)#Normalization
#pitch-angle distribution into magnetic mirror
#Ring-beam
#temperature
T=50#eV
dPl=sqrt(2.*T/511000) 
dtheta=0.1#angle spread
R=10#R=B/B_0 where B is the magnetic field field in area of interest, B_0 -- the magnetic field at the beam generation point
#integration limits
Min_pl=1.5
Max_pl=3.2
Min_pt=0
Max_pt=2.5
Nl=200#longitude
Nt=200#transverse
Ng=200#G-integrals

Distr_type=2# Ring-beam distribution
BeamParamsHotRB=[Distr_type,nb,me,Ze,Omega,vb,1,dPl,dtheta,R]#Ring-beam distribution
IntParamsBeamRB=[Min_pl,Max_pl,Nl,Min_pt,Max_pt,Nt,Ng,DevProp]#Integration parameters 
pyMake_Distribution_Norm(BeamParamsHotRB,IntParamsBeamRB)#Normalization

#plasma
nP=1-nb#relative density
vp=-vb*nb/nP#velocity 
me=1#mass
Ze=1#charge (in units of -e)
pP=vp/sqrt(1-vp*vp)#directed momentum
theta=0#angle between vb and magnetic field
#temperature
Te=100#eV
dPt=sqrt(2.*Te/511000)  
dPl=sqrt(2.*Te/511000)  
#integration limits
Min_pl=pP-5*dPl
Max_pl=pP+5*dPl
Min_pt=0
Max_pt=5*dPt
Nl=100#longitude
Nt=100#transverse
Ng=200#G-integrals

PlasmaParamsCold=[0,nP,me,Ze,Omega,vp]#Cold

Distr_type=1# Maxwellian distribution

PlasmaParamsMaxw=[Distr_type,nP,me,Ze,Omega,vp,1,theta,dPl,dPt]
IntParamsPlasma=[Min_pl,Max_pl,Nl,Min_pt,Max_pt,Nt,Ng,DevProp]
pyMake_Distribution_Norm(PlasmaParamsMaxw,IntParamsPlasma)#Normalization

