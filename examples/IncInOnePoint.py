import sys
sys.path.append('/opt/DispLib/lib/')
import numpy as np
import cmath
from time import time
import matplotlib.colors as col
FontSize=20
from scipy import optimize
import os



#theoretical beam-plasma increment
def TheorInc(nb,kl,kt,vb):
	gamma=1/sqrt(1-vb*vb)
	k2=(kl*kl+kt*kt)
	return (sqrt(3)/2**(4/3))*((nb/gamma)**(1/3))*(kl*kl/(k2*gamma*gamma)+kt*kt/k2)**(1/3)
TheorIncV=np.vectorize(TheorInc)



from DispCython import pyEps
from DispCython import pyDispersion
from DispCython import pyVacEps
from DispCython import pyPrintEps
from DispCython import FUNC
from DispCython import PrintAllCUDAdevices
from DispCython import PrintCUDAdeviceProp
from DispCython import pyNewtonsMethod

from Params import *
#PlasmaParamsCold -- Cold plasma
#PlasmaParamsMaxw -- Maxwellian plasma
#IntParamsPlasma -- Integration parametrs of  Maxwellian plasma

#beam
#BeamParamsCold -- Cold beam
#BeamParamsHotMaxw -- Maxwellian distribution
#IntParamsBeamMaxw -- Integration parameters 
#BeamParamsHotMoon -- Moon distribution
#IntParamsBeamMoon -- Integration parameters 

#FileName='IncMapCold'
#ParticleParams=[PlasmaParamsCold,BeamParamsCold]
#IntParams=[[],[]]

FileName=''
#Calculate on GPU or CPU
tr_use_GPU=1
if tr_use_GPU!=1:#CPU
#use all available CPU threads
	FileName='IncMapMaxwellBeamCPU'
	PrintAllCUDAdevices()
else:
#use GPU number 0
	FileName='IncMapMaxwellBeamGPU'
	PrintAllCUDAdevices()
	print('Device number',IntParamsPlasma[7],'will be used')
	PrintCUDAdeviceProp(IntParamsPlasma[7])

#initial point
kt0=0.02
kl0=round(1/vb,2)
print('kl0=',kl0)
print('kt0=',kt0)
w0=complex(0.98,TheorInc(nb,kl0,0,vb))
print('Theor w=',w0)


#construct Param arrays
ParticleParams=[PlasmaParamsCold,BeamParamsHotMaxw]#Cold plasma with Maxwellian Beam
IntParams=[[],IntParamsBeamMaxw]

Tol=1e-5#precision

Args=[kl0,kt0,ParticleParams,IntParams]
w1=optimize.root(FUNC,x0=[w0.real,w0.imag],args=Args,tol=Tol, jac=None, method='hybr').x
w1=complex(w1[0],w1[1])
print('Numerical w=',w1)

MaxIterNum=20#maximum number of itrations
w2=pyNewtonsMethod(w0.real,w0.imag,kl0, kt0,ParticleParams,IntParams, Tol,MaxIterNum)
print('Numerical Newton w=',w2)
