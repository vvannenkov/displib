import sys
sys.path.append('/opt/DispLib/lib/')
import numpy as np
import cmath
from time import time
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import rc
from matplotlib import cm
import matplotlib.colors as col
FontSize=20
from scipy import optimize
import os


phasecdict = {'red':(
(0.0, 1, 1),#white
(0.15, 0, 0),#0096ff
(0.35, 0, 0),#blue
(0.55, 1, 1),#ff9600
(0.75, 1, 1),
(1.0, 0.5859375, 0.5859375)),

'green':   (
(0.0, 1, 1),#white
(0.15, 0.5859375, 0.5859375),#0096ff
(0.35, 0, 0),#blue
(0.55, 0.5859375, 0.5859375),#ff9600
(0.75, 0, 0),
(1.0, 0, 0)),

'blue':    (
(0.0, 1, 1),#white
(0.15, 1, 1),#0096ff
(0.35, 1, 1),#blue
(0.55, 0, 0),#ff9600
(0.75, 0.5859375, 0.5859375),
(1.0, 0, 0))}
cm = col.LinearSegmentedColormap('phase',phasecdict,N=1024,gamma=1)

#theoretical beam-plasma increment
def TheorInc(nb,kl,kt,vb):
	gamma=1/sqrt(1-vb*vb)
	k2=(kl*kl+kt*kt)
	return (sqrt(3)/2**(4/3))*((nb/gamma)**(1/3))*(kl*kl/(k2*gamma*gamma)+kt*kt/k2)**(1/3)
TheorIncV=np.vectorize(TheorInc)

fig = plt.figure(figsize=(16, 8))
gs =gridspec.GridSpec(1,2)

from DispCython import pyEps
from DispCython import pyDispersion
from DispCython import pyVacEps
from DispCython import pyPrintEps
from DispCython import FUNC
from DispCython import PrintAllCUDAdevices
from DispCython import PrintCUDAdeviceProp
from DispCython import pyNewtonsMethod

from Params import *
#PlasmaParamsCold -- Cold plasma
#PlasmaParamsMaxw -- Maxwellian plasma
#IntParamsPlasma -- Integration parametrs of  Maxwellian plasma

#beam
#BeamParamsCold -- Cold beam
#BeamParamsHotMaxw -- Maxwellian distribution
#IntParamsBeamMaxw -- Integration parameters 
#BeamParamsHotRB -- Ring-beam distribution
#IntParamsBeamRB -- Integration parameters 

#FileName='IncMapCold'
#ParticleParams=[PlasmaParamsCold,BeamParamsCold]
#IntParams=[[],[]]

FileName=''
#Calculate on GPU or CPU
tr_use_GPU=1
if tr_use_GPU!=1:#CPU
#use all available CPU threads
	FileName='IncMapMaxwellBeamCPU'
	PrintAllCUDAdevices()
else:
#use GPU number 0
	FileName='IncMapMaxwellBeamGPU'
	PrintAllCUDAdevices()
	print('Device number',IntParamsPlasma[7],'will be used')
	PrintCUDAdeviceProp(IntParamsPlasma[7])

#construct Param arrays

ParticleParams=[PlasmaParamsCold,BeamParamsHotMaxw]#Cold plasma with Maxwellian Beam
IntParams=[[],IntParamsBeamMaxw]


dirRes="./IncMap"
if not os.path.exists(dirRes):
    os.makedirs(dirRes)



fRes  = open(dirRes+"/"+FileName+".txt", 'w',1)
fRes.write("#kt	kl	w.real	Inc	time,s\n")


MinInc=0.001#minimal increment
Tol=1e-3#precision

dkl=0.02#grid step in k-space
dkt=dkl
NmaxKT=100
NmaxKL=100
fp={}
fp['w'] = np.empty([NmaxKT,NmaxKL],dtype=np.complex128)
fp['kl'] = np.empty([NmaxKT,NmaxKL])
fp['kt'] =  np.empty([NmaxKT,NmaxKL])
fp['tr_Init'] = np.empty([NmaxKT,NmaxKL],dtype=bool)
fp['tr_Calc'] = np.empty([NmaxKT,NmaxKL],dtype=bool)

wRe=[]
wIm=[]
wReCold=[]
wImCold=[]

#initial point
kt0=0.02
kl0=round(1/vb,2)
print('kl0=',kl0)
w0=complex(0.98,TheorInc(nb,kl0,0,vb))
print('w0=',w0)



#make grid
MinKL=0
MinKT=0
MaxKL=MinKL+NmaxKL*dkl
MaxKT=MinKT+NmaxKT*dkt
for kl_n in range(0,NmaxKL):
	for kt_n in range(0,NmaxKT):
		fp['kl'][kt_n][kl_n]=MinKL+kl_n*dkl
		fp['kt'][kt_n][kl_n]=MinKT+kt_n*dkt
		fp['w'][kt_n][kl_n]=complex(0,0)
		fp['tr_Init'][kt_n][kl_n]=False#trigger that the point was a starting point
		fp['tr_Calc'][kt_n][kl_n]=False#trigger that the point has been calculated
		

kl0_n=int((kl0-MinKL)/dkl)
kt0_n=int((kt0-MinKT)/dkl)


Args=[fp['kl'][kt0_n][kl0_n],fp['kt'][kt0_n][kl0_n],ParticleParams,IntParams]

w1=optimize.root(FUNC,x0=[w0.real,w0.imag],args=Args,tol=Tol, jac=None, method='hybr').x
print('w1=',w1)


fp['w'][kt0_n][kl0_n]=complex(w1[0],w1[1])
fp['tr_Calc'][kt0_n][kl0_n]=True

pr=True#trigger
StartTime=time()
while pr==True:
	tr_f=False
	for kt_n in range(0,NmaxKT):
		for kl_n in range(0,NmaxKL):
			if (fp['tr_Init'][kt_n][kl_n]==False) and (fp['tr_Calc'][kt_n][kl_n]==True):#try to find non calculated point
				kl0_n=kl_n
				kt0_n=kt_n
				tr_f=True
				break
		if tr_f==True:
			break
	if(tr_f==False):	#went through the whole grid and didn't find one that wasn't calculated
		pr=False
		break
	#on exit the number of the first point that was not the starting point, but was a calculated one.	
	kl0=fp['kl'][kt0_n][kl0_n]
	kt0=fp['kt'][kt0_n][kt0_n]
	w0=fp['w'][kt0_n][kl0_n]

	if(pr==True):
		for kt_n in range(kt0_n-1,kt0_n+1+1):
			for kl_n in range(kl0_n-1,kl0_n+1+1):
				if kt_n>0 and kl_n>0 and kt_n<NmaxKT and kl_n<NmaxKL:
					if fp['tr_Calc'][kt_n][kl_n]==False:#not yet calculated
						t0 = time()
						kl=fp['kl'][kt0_n][kl0_n]
						kt=fp['kt'][kt0_n][kl0_n]
						Args[0]=fp['kl'][kt0_n][kl0_n]
						Args[1]=fp['kt'][kt0_n][kl0_n]

						#w1=optimize.root(FUNC,x0=[w0.real,w0.imag],args=Args,tol=Tol, jac=None, method='hybr').x
						#w=complex(w1[0],w1[1])
						#Newton method
						w=pyNewtonsMethod(w0.real,w0.imag,kl0, kt0,ParticleParams,IntParams, Tol,Iter_max=20)

						if(w.imag>=MinInc):
							fp['w'][kt_n][kl_n]=w
							fp['tr_Calc'][kt_n][kl_n]=True#marked the point as calculated
							t1 = time()
							print('kt=',fp['kt'][kt_n][kl_n],'kl=',fp['kl'][kt_n][kl_n],'w=',fp['w'][kt_n][kl_n],' time %f' %(t1-t0))
							fRes.write(str(fp['kt'][kt_n][kl_n])+'	'+str(fp['kl'][kt_n][kl_n])+'	'+str(fp['w'][kt_n][kl_n].real)+'	'+str(fp['w'][kt_n][kl_n].imag)+'	'+str((t1-t0))+'\n')
							
						if(w.imag<MinInc):
							fp['tr_Init'][kt_n][kl_n]=True;#to avoid taking a point where there is no or little solution as a starting point
							
		fp['tr_Init'][kt0_n][kl0_n]=True#marked it as used as the starting point
		
FinishTime=time()

print('Total Calc Time for',FileName,'is',FinishTime-StartTime)

fRes.close()

np.save(dirRes+"/"+FileName, fp)


def SubPlot(x,y,fig):
	return fig.add_subplot(gs[y,x])


ax=fig.add_subplot(gs[0,0])
im=ax.imshow(fp['w'].real,cmap=cm,vmin=0.001,origin='lower',aspect='auto',extent=[MinKL,MaxKL,MinKT,MaxKT])

            
ax.set_xlabel(r'$k_\parallel\cdot c/\omega_p$',fontsize=FontSize)
ax.set_ylabel(r'$k_\perp\cdot c/\omega_p$',fontsize=FontSize)


ax=fig.add_subplot(gs[0,1])
im=ax.imshow(fp['w'].imag,cmap=cm,vmin=0.001,origin='lower',aspect='auto',extent=[MinKL,MaxKL,MinKT,MaxKT])#,interpolation='kaiser'

            
ax.set_xlabel(r'$k_\parallel\cdot c/\omega_p$',fontsize=FontSize)
ax.set_ylabel(r'$k_\perp\cdot c/\omega_p$',fontsize=FontSize)

plt.savefig(dirRes+'/'+FileName+'.pdf',dpi=150,format='pdf')
#plt.show()
