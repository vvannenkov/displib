DispLib
=======

**Designed by [Vladimir Annenkov](mailto:annenkov.phys@gmail.com)**


The shared library which provides exact calculations of the full unstable spectrum of plasma oscillations in the framework of relativistic kinetic theory for arbitrary magnetic fields and particle distributions.

Based on the article:  I. V. Timofeev and V. V. Annenkov, *Exact kinetic theory for the instability of an electron beam in a hot magnetized plasma,* Physics of Plasmas, vol. 20, no. 9, p. 092123, Sep. 2013. [doi:10.1063/1.4823722](https://www.doi.org/10.1063/1.4823722)

Requirements
-----------

* C++ compiler with C++14 support 
* Nvidia GPU with compute capability not less then 6.0 (Pascal  micro-architecture and higher)

Compilation
-----------
Makefile has several targets, each of them uses the main Makefile from the *srcDisp* directory. The LIB_DIR variable specifies the location where to install the library (default is /opt/DispLib)

1.  **make sharedMP** --- Compile a library for use on the CPU (using OpenMP);
2.  **make sharedCUDA** --- Compile a library for use on the Nvidia GPU (provide calculations on CPU (with OpenMP) and on GPU (with CUDA));
3.  **make cython_wrap** --- Compile a cython wrapper;
4.  **make cyCUDA** --- targets: sharedCUDA + cython_wrap;
5.  **make cyMP** ---  targets: sharedMP + cython_wrap.

Default target is **cyMP**



Usage
-----------

Examples of use with python can be found in the *examples* folder.

Manual
-----------

Can be found in the folder "Manual".
