// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

#include <complex>
#include <cassert> // for assert()
#include <omp.h>//OpenMP
//#include <iostream>		// std::cout

using namespace std;
using namespace std::complex_literals;

#include "ClassEpsilon.hpp"
//подынтегральные выражения, необхоимые привычислении компонент тензора в рамках кинетической теории
#include "HotEpsilon.hpp"

#include "DispLocal.hpp"


//однопоточное вычисление тензора
extern "C" void HotEpsSerial(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res)
{

	const complex<double> I=complex<double>(0.,1.0);
	const complex<double> CNull=complex<double>(0.,0.);
	double  DTDISTRIB=0.,DLDISTRIB=0.;
	double Norm;//нормировка распределения

	Norm=PartParams[6];
	epsilon eps;
	
	complex<double> g0=CNull,g1=CNull,g2=CNull,g11=CNull;
	complex<double> Dg0=CNull,Dg1=CNull,Dg2=CNull;

	//параметры частиц
	double n=PartParams[1];//концентрация
	double m=PartParams[2];//масса
	double Z=PartParams[3];//зарядовое число (со знаком, определеющим знак заряда)
	double Omega=PartParams[4]*Z/m;//внеешнее магнитное поле

	double Min_pl=IntParams[0];//минимальный имульс, до которого интегрировать 
	double Max_pl=IntParams[1];//максимальный имульс, до которого интегрировать 
	size_t Nl=IntParams[2];//Количество отрезков интегрирования

	//предел по поперечному импульсу
	//количество отрезков по поперечному импульсу
	double Min_pt=IntParams[3];//минимальный имульс, до которого интегрировать 
	double Max_pt=IntParams[4];//максимальный имульс, до которого интегрировать 
	size_t Nt=IntParams[5];//Количество отрезков интегрирования

	//количество отрезков интегрирования в G-интегралах
	size_t Ng=IntParams[6];//Количество отрезков интегрирования
	
	//шаги интегрирования
	double hpl=(Max_pl-Min_pl)/double(Nl);
	double hpt=(Max_pt-Min_pt)/double(Nt);

	complex<double> w=complex<double>(w_real,w_imag);
	

	double PL=0.,PT=0.;//продольный поперечный импульс, использ-ся при интегрировании
	complex<double> C0,DC0,a,U;
	double gamma=0.;
	double z=0.;
	double TempDistr[3];

	for(size_t l=0;l<Nl;++l)
	{
		PL=Min_pl+(l+double(0.5))*hpl;
		for(size_t t=0;t<Nt;++t)
		{
			PT=Min_pt+(t+double(0.5))*hpt;
			//импульс обезразмерен на массу покоя частиц
			gamma=sqrt(double(1.)+(PT)*(PT)+(PL)*(PL));
			a=(gamma*w-kl*(PL))/Omega;
			z=kt*(PT)/Omega;
			
			//вычисляем функцию распределения и её производные
			Distribution(PartParams,Norm,PL,PT,TempDistr);
			//DISTRIB=TempDistr[0];
			DLDISTRIB=TempDistr[1];
			DTDISTRIB=TempDistr[2];
			U=(w-kl*PL/gamma)*DTDISTRIB+kl*PT*DLDISTRIB/gamma;


			C0 = double(1.)/(exp(-I*double(2.)*M_PI*a)-double(1.));

			DC0 = (I*double(2.)*M_PI*gamma*C0*C0*exp(-I*double(2.)*double(M_PI)*a)/Omega);
			CalcAllGint(Omega,gamma,a,z,Ng,g0,g1,g2,Dg0,Dg1,Dg2);//calculate all G-integrals and their derivatives

			eps.XX+=FEPSXX1;
			eps.YY+=FEPSYY1;
			eps.ZZ+=FEPSZZ1;
	
			eps.XY+=FEPSXY1;
			eps.XZ+=FEPSXZ1;
			eps.YZ+=FEPSYZ1;
			//производные
			eps.XXD+=FDEPSXX1;
			eps.YYD+=FDEPSYY1;
			eps.ZZD+=FDEPSZZ1;

			eps.XYD+=FDEPSXY1;
			eps.XZD+=FDEPSXZ1;
			eps.YZD+=FDEPSYZ1;

		}

	}//конец цикла - интегрирования

		eps=eps*hpt*hpl;
	

		eps.XX=-(double(2.)*M_PI*Omega/(w*w*kt*kt))*eps.XX;
		eps.YY=-eps.XX-(double(2.)*M_PI*I/(w*w*Omega))*eps.YY;
		eps.ZZ=(double(2.)*M_PI/(w*w))*eps.ZZ;
		eps.XY=-(M_PI*I/(w*w*Omega))*eps.XY;
		eps.XZ=-(double(2.)*M_PI/(w*w*kt))*eps.XZ;
		eps.YZ=(I*M_PI*kt/(w*w*Omega*Omega))*eps.YZ;

		eps.ZZD=(-double(2.)/w)*eps.ZZ+(double(2.)*M_PI/(w*w))*eps.ZZD;
		eps.XXD=(-double(2.)/w)*eps.XX-(double(2.)*M_PI*Omega/(w*w*kt*kt))*eps.XXD;
		eps.YYD=(double(2.)/w)*(double(2.)*M_PI*I/(w*w*Omega))*eps.YY-eps.XXD-(double(2.)*M_PI*I/(w*w*Omega))*eps.YYD;
		eps.XYD=(-double(2.)/w)*eps.XY-(M_PI*I/(w*w*Omega))*eps.XYD;
		eps.XZD=(-double(2.)/w)*eps.XZ-(double(2.)*M_PI/(w*w*kt))*eps.XZD;
		eps.YZD=(-double(2.)/w)*eps.YZ+(I*M_PI*kt/(w*w*Omega*Omega))*eps.YZD;

		eps.YX=-eps.XY;
		eps.ZY=-eps.YZ;
		eps.ZX=eps.XZ;

		eps.YXD=-eps.XYD;
		eps.ZXD=eps.XZD;
		eps.ZYD=-eps.YZD;
		
		EpsToRes(Res,n*eps);

}




	

//параллельный вариант
//распараллелено интегрирование по импульсу
extern "C" void HotEpsMP(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res)
{
	//проверка обязательных параметров
	CheckIntParams(IntParams);	
	CheckPartParams(PartParams);	
	
	const complex<double> I=complex<double>(0.,1.0);
	const complex<double> CNull=complex<double>(0.,0.);
	double Norm;//нормировка распределения

	Norm=PartParams[6];	

	//параметры частиц
	double n=PartParams[1];//концентрация
	double m=PartParams[2];//масса
	double Z=PartParams[3];//зарядовое число (со знаком, определеющим знак заряда)
	double Omega=PartParams[4]*Z/m;//внеешнее магнитное поле

	double Min_pl=IntParams[0];//минимальный имульс, до которого интегрировать 
	double Max_pl=IntParams[1];//максимальный имульс, до которого интегрировать 
	size_t Nl=IntParams[2];//Количество отрезков интегрирования

	//предел по поперечному импульсу
	//количество отрезков по поперечному импульсу
	double Min_pt=IntParams[3];//минимальный имульс, до которого интегрировать 
	double Max_pt=IntParams[4];//максимальный имульс, до которого интегрировать 
	size_t Nt=IntParams[5];//Количество отрезков интегрирования

	//количество отрезков интегрирования в G-интегралах
	size_t Ng=IntParams[6];//Количество отрезков интегрирования
	
	//шаги интегрирования
	double dpl=(Max_pl-Min_pl)/double(Nl);
	double dpt=(Max_pt-Min_pt)/double(Nt);

	complex<double> w=complex<double>(w_real,w_imag);

	int ThreadNum=IntParams[7];//сколько потоков использовать
	if(ThreadNum==0)//use all threads
		ThreadNum=omp_get_max_threads();

	assert(ThreadNum >= 1 && "Wrong amount of MP threads"); 

	double epsArray[36]={};
	#pragma omp parallel num_threads(ThreadNum)  reduction(+:epsArray[:36])
	{
		int tid = omp_get_thread_num();

		double  DISTRIB=0.,DTDISTRIBCOEF=0.,DLDISTRIBCOEF=0.,DTDISTRIB=0.,DLDISTRIB=0.;
		epsilon eps;
		double PL=0.,PT=0.;//продольный поперечный импульс, использ-ся при интегрировании
		complex<double> C0,DC0,a,U;
		double gamma=0.;
		double z=0.;
		double TempDistr[3];
		complex<double> g0=CNull,g1=CNull,g2=CNull,g11=CNull;
		complex<double> Dg0=CNull,Dg1=CNull,Dg2=CNull;
		for (size_t index = tid; index < Nl*Nt; index+=ThreadNum) 
		{
				PL =Min_pl+ (index / Nt+double(0.5))*dpl;
				PT =Min_pt+(index % Nl+double(0.5))*dpt;
		
				//импульс обезразмерен на массу покоя частиц
				gamma=sqrt(double(1.)+(PT)*(PT)+(PL)*(PL));
				a=(gamma*w-kl*(PL))/Omega;
				z=kt*(PT)/Omega;
				
				//вычисляем функцию распределения и её производные
				Distribution(PartParams,Norm,PL,PT,TempDistr);
				DISTRIB=TempDistr[0];
				DLDISTRIB=TempDistr[1];
				DTDISTRIB=TempDistr[2];
				//printf("pl=%g pt=%g %g %g %g\n",PL,PT,DISTRIB,DLDISTRIB,DTDISTRIB);
				U=(w-kl*PL/gamma)*DTDISTRIB+kl*PT*DLDISTRIB/gamma;


				C0 = double(1.)/(exp(-I*double(2.)*M_PI*a)-double(1.));

				DC0 = (I*double(2.)*M_PI*gamma*C0*C0*exp(-I*double(2.)*double(M_PI)*a)/Omega);
				CalcAllGint(Omega,gamma,a,z,Ng,g0,g1,g2,Dg0,Dg1,Dg2);//calculate all G-integrals and their derivatives


				eps.XX+=FEPSXX1;
				eps.YY+=FEPSYY1;
				eps.ZZ+=FEPSZZ1;
		
				eps.XY+=FEPSXY1;
				eps.XZ+=FEPSXZ1;
				eps.YZ+=FEPSYZ1;
				//производные
				eps.XXD+=FDEPSXX1;
				eps.YYD+=FDEPSYY1;
				eps.ZZD+=FDEPSZZ1;

				eps.XYD+=FDEPSXY1;
				eps.XZD+=FDEPSXZ1;
				eps.YZD+=FDEPSYZ1;
			

		}//конец цикла - интегрирования

		eps=eps*dpt*dpl;
		EpsToRes(epsArray,eps);
	}

	epsilon FinEps;
	ResToEps(epsArray,FinEps);
	
	FinEps.XX=-(double(2.)*M_PI*Omega/(w*w*kt*kt))*FinEps.XX;
	FinEps.YY=-FinEps.XX-(double(2.)*M_PI*I/(w*w*Omega))*FinEps.YY;
	FinEps.ZZ=(double(2.)*M_PI/(w*w))*FinEps.ZZ;
	FinEps.XY=-(M_PI*I/(w*w*Omega))*FinEps.XY;
	FinEps.XZ=-(double(2.)*M_PI/(w*w*kt))*FinEps.XZ;
	FinEps.YZ=(I*M_PI*kt/(w*w*Omega*Omega))*FinEps.YZ;

	FinEps.ZZD=(-double(2.)/w)*FinEps.ZZ+(double(2.)*M_PI/(w*w))*FinEps.ZZD;
	FinEps.XXD=(-double(2.)/w)*FinEps.XX-(double(2.)*M_PI*Omega/(w*w*kt*kt))*FinEps.XXD;
	FinEps.YYD=(double(2.)/w)*(double(2.)*M_PI*I/(w*w*Omega))*FinEps.YY-FinEps.XXD-(double(2.)*M_PI*I/(w*w*Omega))*FinEps.YYD;
	FinEps.XYD=(-double(2.)/w)*FinEps.XY-(M_PI*I/(w*w*Omega))*FinEps.XYD;
	FinEps.XZD=(-double(2.)/w)*FinEps.XZ-(double(2.)*M_PI/(w*w*kt))*FinEps.XZD;
	FinEps.YZD=(-double(2.)/w)*FinEps.YZ+(I*M_PI*kt/(w*w*Omega*Omega))*FinEps.YZD;


	FinEps.YX=-FinEps.XY;
	FinEps.ZY=-FinEps.YZ;
	FinEps.ZX=FinEps.XZ;

	FinEps.YXD=-FinEps.XYD;
	FinEps.ZXD=FinEps.XZD;
	FinEps.ZYD=-FinEps.YZD;

	EpsToRes(Res,n*FinEps);

}



