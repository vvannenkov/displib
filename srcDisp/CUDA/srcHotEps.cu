// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

//различные вспомогательные cuda-функции
#include <stdio.h>
#include <complex>
#include <iostream>		// std::cout
#include <cassert> // для assert()

#include <cuComplex.h>
#include "./cudaComplex.h"

using namespace std;
using namespace std::complex_literals;
#include "./cudaFuncs.h"
#include "./srcGfunc.cu"

#include "ClassCuEpsilon.cu"

#include "../ClassEpsilon.hpp"

//подынтегральные выражения, необхоимые при вычислении компонент тензора в рамках кинетической теории
#include "../HotEpsilon.hpp"

#include "../DispLocal.hpp"



#include "srcDistributions.cu"


//распараллеливание только по импульсу
__global__ void  cuHotPlasmaP(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,cuDoubleComplex *Res)
{	
	int tid=blockIdx.x*(blockDim.x)+threadIdx.x;//линеаризованная координата
	int Nl=IntParams[2];//Количество отрезков интегрирования
	int Nt=IntParams[5];//Количество отрезков интегрирования

	//printf("tid=%d Nl=%d Nt=%d\n",int(tid),int(Nl),int(Nt));

	if(tid<Nl*Nt)//каждая нить вычисляет одну точку в пространстве импульсов
	{
		
		const cuDoubleComplex I=make_cuDoubleComplex(0.,1.0);
		const cuDoubleComplex CNull=make_cuDoubleComplex(0.,0.);
		double Norm;//нормировка распределения

		Norm=PartParams[6];	

		//параметры частиц
	//	double n=PartParams[1];//концентрация
		double m=PartParams[2];//масса
		double Z=PartParams[3];//зарядовое число (со знаком, определеющим знак заряда)
		double Omega=PartParams[4]*Z/m;//внеешнее магнитное поле

		double Min_pl=IntParams[0];//минимальный имульс, до которого интегрировать 
		double Max_pl=IntParams[1];//максимальный имульс, до которого интегрировать 

		//предел по поперечному импульсу
		//количество отрезков по поперечному импульсу
		double Min_pt=IntParams[3];//минимальный имульс, до которого интегрировать 
		double Max_pt=IntParams[4];//максимальный имульс, до которого интегрировать 


		//количество отрезков интегрирования в G-интегралах
		size_t Ng=IntParams[6];//Количество отрезков интегрирования
		
		//шаги интегрирования
		double dpl=(Max_pl-Min_pl)/double(Nl);
		double dpt=(Max_pt-Min_pt)/double(Nt);

		cuDoubleComplex w=make_cuDoubleComplex(w_real,w_imag);

		double  DTDISTRIB=0.,DLDISTRIB=0.;
		
		cu_epsilon eps;
		
		double PL=0.,PT=0.;//продольный поперечный импульс, использ-ся при интегрировании
		cuDoubleComplex C0,DC0,a,U;
		double gamma=0.;
		double z=0.;
		double TempDistr[3];
		cuDoubleComplex g0=CNull,g1=CNull,g2=CNull;
		cuDoubleComplex Dg0=CNull,Dg1=CNull,Dg2=CNull;

		PL =Min_pl+ (tid / Nt+double(0.5))*dpl;
		PT =Min_pt+(tid % Nl+double(0.5))*dpt;

		//импульс обезразмерен на массу покоя частиц
		gamma=sqrt(double(1.)+(PT)*(PT)+(PL)*(PL));
		a=(gamma*w-kl*(PL))/Omega;
		z=kt*(PT)/Omega;
		
		//вычисляем функцию распределения и её производные
		cuDistribution(PartParams,Norm,PL,PT,TempDistr);
		//DISTRIB=TempDistr[0];
		DLDISTRIB=TempDistr[1];
		DTDISTRIB=TempDistr[2];
		//printf("pl=%g pt=%g %g %g %g\n",PL,PT,DISTRIB,DLDISTRIB,DTDISTRIB);
		U=(w-kl*PL/gamma)*DTDISTRIB+kl*PT*DLDISTRIB/gamma;


		C0 = double(1.)/(exp(-I*double(2.)*M_PI*a)-double(1.));

		DC0 = (I*double(2.)*M_PI*gamma*C0*C0*exp(-I*double(2.)*double(M_PI)*a)/Omega);
		
		GPUCalcAllGint(Omega,gamma,a,z,Ng,g0,g1,g2,Dg0,Dg1,Dg2);//calculate all G-integrals and their derivatives


		eps.XX=FEPSXX1;
		eps.YY=FEPSYY1;
		eps.ZZ=FEPSZZ1;
		

		eps.XY=FEPSXY1;
		eps.XZ=FEPSXZ1;
		eps.YZ=FEPSYZ1;
		//производные
		eps.XXD=FDEPSXX1;
		eps.YYD=FDEPSYY1;
		eps.ZZD=FDEPSZZ1;

		eps.XYD=FDEPSXY1;
		eps.XZD=FDEPSXZ1;
		eps.YZD=FDEPSYZ1;
		eps=eps*dpl*dpt;

		//просуммировать в разделяемой памяти
	/*	atomicAdd(&sh_eps[0],eps.XX);
		atomicAdd(&sh_eps[1],eps.YY);
		atomicAdd(&sh_eps[2],eps.ZZ);
		
		atomicAdd(&sh_eps[3],eps.XY);
		atomicAdd(&sh_eps[4],eps.XZ);
		atomicAdd(&sh_eps[5],eps.YZ);
		
		atomicAdd(&sh_eps[6],eps.XXD);
		atomicAdd(&sh_eps[7],eps.YYD);
		atomicAdd(&sh_eps[8],eps.ZZD);
		
		atomicAdd(&sh_eps[9],eps.XYD);
		atomicAdd(&sh_eps[10],eps.XZD);
		atomicAdd(&sh_eps[11],eps.YZD);*/
		atomicAdd(&Res[0],eps.XX);
		atomicAdd(&Res[1],eps.YY);
		atomicAdd(&Res[2],eps.ZZ);
		
		atomicAdd(&Res[3],eps.XY);
		atomicAdd(&Res[4],eps.XZ);
		atomicAdd(&Res[5],eps.YZ);
		
		atomicAdd(&Res[6],eps.XXD);
		atomicAdd(&Res[7],eps.YYD);
		atomicAdd(&Res[8],eps.ZZD);
		
		atomicAdd(&Res[9],eps.XYD);
		atomicAdd(&Res[10],eps.XZD);
		atomicAdd(&Res[11],eps.YZD);
		
	/*	__syncthreads();

			switch(threadIdx.x)
			{
				case 0:	atomicAdd(&Res[0],sh_eps[0]); break;
				case 1:	atomicAdd(&Res[1],sh_eps[1]); break;
				case 2:	atomicAdd(&Res[2],sh_eps[2]); break;
				case 3:	atomicAdd(&Res[3],sh_eps[3]); break;
				case 4:	atomicAdd(&Res[4],sh_eps[4]); break;
				case 5:	atomicAdd(&Res[5],sh_eps[5]); break;
				case 6:	atomicAdd(&Res[6],sh_eps[6]); break;
				case 7:	atomicAdd(&Res[7],sh_eps[7]); break;
				case 8:	atomicAdd(&Res[8],sh_eps[8]); break;
				case 9:	atomicAdd(&Res[9],sh_eps[9]); break;
				case 10: atomicAdd(&Res[10],sh_eps[10]); break;
				case 11: atomicAdd(&Res[11],sh_eps[11]); break;
		}
*/
		
		__syncthreads();
	
	}
}


//параллельный вариант
//распараллелено интегрирование по импульсу
extern "C" void HotEpsCUDA(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res)
{
	//проверка обязательных параметров
	CheckIntParams(IntParams);	
	CheckPartParams(PartParams);	
	assert(IntParams[7] >= 0 && "Wrong GPU number"); 

	double n=PartParams[1];//концентрация
	double m=PartParams[2];//масса
	double Z=PartParams[3];//зарядовое число (со знаком, определеющим знак заряда)
	double Omega=PartParams[4]*Z/m;//внеешнее магнитное поле
	size_t Nl=IntParams[2];//Количество отрезков интегрирования
	size_t Nt=IntParams[5];//Количество отрезков интегрирования
	int gpu=IntParams[7];//на каком gpu запускать
	const cuDoubleComplex I=make_cuDoubleComplex(0.,1.0);
	cuDoubleComplex w=make_cuDoubleComplex(w_real,w_imag);
	cuDoubleComplex *dev_Res;
	int PartParamNum=Distribution_ParamNum(PartParams);
	double *dev_PartParams;
	double *dev_IntParams;//добавит функцию, возвращащую число параметров распределения.
	cudaMallocManaged(&dev_Res,12*sizeof(cuDoubleComplex));
	cudaMallocManaged(&dev_PartParams,PartParamNum*sizeof(double));
	cudaMallocManaged(&dev_IntParams,7*sizeof(double));
	
	for(int i=0;i<PartParamNum;++i)
		dev_PartParams[i]=PartParams[i];
	for(int i=0;i<7;++i)
		dev_IntParams[i]=IntParams[i];

	SetDevice(gpu);

	cuHotPlasmaP<<<(Nl*Nt+64-1)/64,64>>>(w_real,w_imag,kl, kt,dev_PartParams,dev_IntParams,dev_Res);
	cudaDeviceSynchronize();  

	cu_epsilon FinEps;			
	FinEps.XX=dev_Res[0];
	FinEps.YY=dev_Res[1];
	FinEps.ZZ=dev_Res[2];

	FinEps.XY=dev_Res[3];
	FinEps.XZ=dev_Res[4];
	FinEps.YZ=dev_Res[5];
	//производные
	FinEps.XXD=dev_Res[6];
	FinEps.YYD=dev_Res[7];
	FinEps.ZZD=dev_Res[8];

	FinEps.XYD=dev_Res[9];
	FinEps.XZD=dev_Res[10];
	FinEps.YZD=dev_Res[11];
		
	FinEps.XX=-(double(2.)*M_PI*Omega/(w*w*kt*kt))*FinEps.XX;
	FinEps.YY=-FinEps.XX-(double(2.)*M_PI*I/(w*w*Omega))*FinEps.YY;
	FinEps.ZZ=(double(2.)*M_PI/(w*w))*FinEps.ZZ;
	FinEps.XY=-(M_PI*I/(w*w*Omega))*FinEps.XY;
	FinEps.XZ=-(double(2.)*M_PI/(w*w*kt))*FinEps.XZ;
	FinEps.YZ=(I*M_PI*kt/(w*w*Omega*Omega))*FinEps.YZ;

	FinEps.ZZD=(-double(2.)/w)*FinEps.ZZ+(double(2.)*M_PI/(w*w))*FinEps.ZZD;
	FinEps.XXD=(-double(2.)/w)*FinEps.XX-(double(2.)*M_PI*Omega/(w*w*kt*kt))*FinEps.XXD;
	FinEps.YYD=(double(2.)/w)*(double(2.)*M_PI*I/(w*w*Omega))*FinEps.YY-FinEps.XXD-(double(2.)*M_PI*I/(w*w*Omega))*FinEps.YYD;
	FinEps.XYD=(-double(2.)/w)*FinEps.XY-(M_PI*I/(w*w*Omega))*FinEps.XYD;
	FinEps.XZD=(-double(2.)/w)*FinEps.XZ-(double(2.)*M_PI/(w*w*kt))*FinEps.XZD;
	FinEps.YZD=(-double(2.)/w)*FinEps.YZ+(I*M_PI*kt/(w*w*Omega*Omega))*FinEps.YZD;


	FinEps.YX=-FinEps.XY;
	FinEps.ZY=-FinEps.YZ;
	FinEps.ZX=FinEps.XZ;

	FinEps.YXD=-FinEps.XYD;
	FinEps.ZXD=FinEps.XZD;
	FinEps.ZYD=-FinEps.YZD;

	EpsToRes(Res,n*FinEps);
	
	cudaFree(dev_Res);
	cudaFree(dev_PartParams);
	cudaFree(dev_IntParams);
}

