// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0


//различные вспомогательные cuda-функции
#include <stdio.h>

int getSPcores(cudaDeviceProp devProp)
{  
    int cores = 0;
    int mp = devProp.multiProcessorCount;
    switch (devProp.major){
     case 2: // Fermi
      if (devProp.minor == 1) cores = mp * 48;
      else cores = mp * 32;
      break;
     case 3: // Kepler
      cores = mp * 192;
      break;
     case 5: // Maxwell
      cores = mp * 128;
      break;
     case 6: // Pascal
      if ((devProp.minor == 1) || (devProp.minor == 2)) cores = mp * 128;
      else if (devProp.minor == 0) cores = mp * 64;
      else printf("Unknown device type\n");
      break;
     case 7: // Volta and Turing
      if ((devProp.minor == 0) || (devProp.minor == 5)) cores = mp * 64;
      else printf("Unknown device type\n");
      break;
     case 8: // Ampere
      if (devProp.minor == 0) cores = mp * 64;
      else printf("Unknown device type\n");
      break;
     default:
      printf("Unknown device type\n"); 
      break;
      }
    return cores;
}

extern "C" void DeviceProp(const int gpu)
{
	//https://docs.nvidia.com/cuda/cuda-runtime-api/structcudaDeviceProp.html
	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, gpu);
	printf("Device Number: %d\n", gpu);
	printf("  Device name: %s\n", prop.name);
	printf("  Clock Rate (KHz): %d\n",
		   prop.clockRate);
	printf("  Memory Clock Rate (KHz): %d\n",
		   prop.memoryClockRate);
	printf("  Memory Bus Width (bits): %d\n",
		   prop.memoryBusWidth);
	printf("  Peak Memory Bandwidth (GB/s): %f\n",
		   2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
	printf("  multiProcessorCount : %d\n",  prop.multiProcessorCount);
	printf("  ECCEnabled : %d\n",  prop.ECCEnabled);
	printf("  l2CacheSize (MB): %f\n",  prop.l2CacheSize/1024./1024.);
}

extern "C" void AllDevicesProp()
{	
	int nDevices;
	cudaGetDeviceCount(&nDevices);
	printf("Number of devices=%d\n",nDevices);
	for(int i=0;i<nDevices;++i)
		DeviceProp(i);
}


void SetDevice(const int gpu)
{
	int assigned_device=gpu;

	int used_device;
	// Select the used device:
	if ( cudaSetDevice(assigned_device) != cudaSuccess or
			cudaGetDevice( &used_device ) != cudaSuccess or
			used_device != assigned_device) {
		printf ("Error: unable to set device %d\n", assigned_device);
	}
}
