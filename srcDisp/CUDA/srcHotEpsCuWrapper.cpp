// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0


//обёртка эпсилона для CUDA

extern "C" void HotEpsCUDA(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res);
extern "C" void ColdPlasma(double w_real,double w_imag,double kl, double kt,double* PartParams,double *Res);


extern "C" void Eps(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res)
{
	if(PartParams[0]==0)//cold
		ColdPlasma(w_real,w_imag,kl, kt,PartParams,Res);
	else
		HotEpsCUDA(w_real,w_imag,kl, kt,PartParams,IntParams,Res);
}
