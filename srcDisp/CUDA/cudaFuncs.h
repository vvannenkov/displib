// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

extern int getSPcores(cudaDeviceProp devProp);
extern "C" void DeviceProp(const int gpu);
extern "C" void AllDevicesProp();
void SetDevice(const int gpu);



