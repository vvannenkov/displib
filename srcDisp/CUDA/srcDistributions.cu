// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0



//Нормировку функции распределения считаем внешним параметром (вычисленным численно или аналитически)

#include "../srcDistributions/distrAnysMaxwell.hpp"
#include "../srcDistributions/distrMoon.hpp"

//возвращает значение функции распределения

__host__ __device__ void cuDistribution(double* PartParams,double Norm,double PL,double PT,double *Distrib)
{
	int PartType=PartParams[0];//тип распределения

	#include "../srcDistributions/distrFunc.hpp"

}


