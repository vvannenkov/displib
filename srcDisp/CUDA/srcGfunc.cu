// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0


void CopyComplex(complex<double> &a,cuDoubleComplex b)
{
	a=complex<double>(b.x,b.y);
}  

#include "../Gintegrals.hpp"


__global__ void  cuCalcAllGint(double Omega,double gamP,cuDoubleComplex a,double z,int Ng, cuDoubleComplex *Res)
{

	size_t tid=blockIdx.x*(blockDim.x)+threadIdx.x;//линеаризованная координата

	cuDoubleComplex CNull = make_cuDoubleComplex(0, 0);//комплексный ноль

	if(tid<Ng)
	{
		cuDoubleComplex I = make_cuDoubleComplex(0, 1.);//мнимая единица

		double phi=0,hG=double(2.)*double(M_PI)/double(Ng);
		double SIN, COS,Bessel;
		cuDoubleComplex EXP;
	
		cuDoubleComplex g0,g1,g2,Dg0,Dg1,Dg2;

		

		phi=tid*hG+double(0.5)*hG;
		sincos(phi,&SIN, &COS);
		Bessel=j0(double(2.)*z*sin(phi*double(0.5)));
		EXP=exp(-I*a*(phi));
		
		g0=hG*FG0;
		g1=hG*FG1;
		g2=hG*FG2;
		
		Dg0=hG*FDG0;
		Dg1=hG*FDG1;
		Dg2=hG*FDG2;
		
		//суммирая сразу в глобальную получилось быстрее о_О
		atomicAdd(&Res[0],g0);
		atomicAdd(&Res[1],g1);
		atomicAdd(&Res[2],g2);

		atomicAdd(&Res[3],Dg0);
		atomicAdd(&Res[4],Dg1);
		atomicAdd(&Res[5],Dg2);

		

	}

	__syncthreads();
}

void  CalcAllGintCUDA(double Omega,double gamma,complex<double> a,double z,int Ng,complex<double> &g0,complex<double> &g1, complex<double> &g2,complex<double> &Dg0,complex<double> &Dg1, complex<double> &Dg2)
{

cuDoubleComplex cu_a= make_cuDoubleComplex(a.real(), a.imag());
cuDoubleComplex* Res;
cudaMallocManaged(&Res,6*sizeof(cuDoubleComplex));

cudaMemset(Res,0,6*sizeof(double));
int CUDA_THREAD_NUM=64;
int BlockNumber=(Ng+CUDA_THREAD_NUM-1)/CUDA_THREAD_NUM;
cuCalcAllGint<<<BlockNumber,CUDA_THREAD_NUM>>>(Omega,gamma,cu_a,z,Ng,Res);
cudaDeviceSynchronize();  

CopyComplex(g0,Res[0]);
CopyComplex(g1,Res[1]);
CopyComplex(g2,Res[2]);
CopyComplex(Dg0,Res[3]);
CopyComplex(Dg1,Res[4]);
CopyComplex(Dg2,Res[5]);

cudaFree(Res);

}


__device__ void  GPUCalcAllGint(double Omega,double gamP,cuDoubleComplex a,double z,int Ng,cuDoubleComplex &g0,cuDoubleComplex &g1, cuDoubleComplex &g2,cuDoubleComplex &Dg0,cuDoubleComplex &Dg1, cuDoubleComplex &Dg2)
{

	const cuDoubleComplex I=make_cuDoubleComplex(0.,1.0);//мнимая единица
	const cuDoubleComplex CNull=make_cuDoubleComplex(0.,0.);//комплексный ноль

	double phi=0,hG=double(2.)*double(M_PI)/double(Ng);
	cuDoubleComplex g0t=CNull,g1t=CNull, g2t=CNull;
	double SIN, COS,Bessel;
	cuDoubleComplex EXP;

	cuDoubleComplex Dg0t=CNull,Dg1t=CNull, Dg2t=CNull;
	for(int i=0;i<Ng;++i)
	{
		phi=(i+double(0.5))*hG;
		sincos(phi,&SIN, &COS);
		Bessel=j0(double(2.)*z*sin(phi*0.5));
		EXP=exp(-I*a*(phi));
		g0t=g0t+FG0;
		g1t=g1t+FG1;
		g2t=g2t+FG2;
		
		Dg0t=Dg0t+FDG0;
		Dg1t=Dg1t+FDG1;
		Dg2t=Dg2t+FDG2;
		
	}
	g0=hG*g0t;
	g1=hG*g1t;
	g2=hG*g2t;
	Dg0=hG*Dg0t;
	Dg1=hG*Dg1t;
	Dg2=hG*Dg2t;
}
