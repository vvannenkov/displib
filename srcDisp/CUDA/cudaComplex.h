// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

#pragma once

__device__ __host__ cuDoubleComplex  operator*(cuDoubleComplex a, cuDoubleComplex b) { return cuCmul(a,b); }
__device__ __host__ cuDoubleComplex  operator+(cuDoubleComplex a, cuDoubleComplex b) { return cuCadd(a,b); }
__device__ __host__ cuDoubleComplex  operator/(cuDoubleComplex a, cuDoubleComplex b) { return cuCdiv(a,b); }
__device__ __host__ cuDoubleComplex  operator-(cuDoubleComplex a, cuDoubleComplex b) { return make_cuDoubleComplex(cuCreal(a)-cuCreal(b),cuCimag(a)-cuCimag(b));}
__device__ __host__ cuDoubleComplex  operator-(cuDoubleComplex a) { return make_cuDoubleComplex(-cuCreal(a),-cuCimag(a));}
//операции с обычными вещественными числами
__device__ __host__ cuDoubleComplex  operator*(cuDoubleComplex a, double b) { return make_cuDoubleComplex(cuCreal(a)*b,cuCimag(a)*b); }
__device__ __host__ cuDoubleComplex  operator/(cuDoubleComplex a, double b) { return make_cuDoubleComplex(cuCreal(a)/b,cuCimag(a)/b); }
__device__ __host__ cuDoubleComplex  operator+(cuDoubleComplex a, double b) { return make_cuDoubleComplex(cuCreal(a)+b,cuCimag(a)); }
__device__ __host__ cuDoubleComplex  operator-(cuDoubleComplex a, double b) { return make_cuDoubleComplex(cuCreal(a)-b,cuCimag(a)); }


__device__ __host__ cuDoubleComplex  operator*(double a, cuDoubleComplex b) { return make_cuDoubleComplex(cuCreal(b)*a,cuCimag(b)*a); }
__device__ __host__ cuDoubleComplex  operator/(double a, cuDoubleComplex b) { return cuCdiv(make_cuDoubleComplex(a,0),b); }
__device__ __host__ cuDoubleComplex  operator+(double a, cuDoubleComplex b) { return make_cuDoubleComplex(cuCreal(b)+a,cuCimag(b)); }
__device__ __host__ cuDoubleComplex  operator-(double a, cuDoubleComplex b) { return make_cuDoubleComplex(a-cuCreal(b),-cuCimag(b)); }

__device__ __host__ cuDoubleComplex  exp(cuDoubleComplex a) {

double x=cuCreal(a);
double y=cuCimag(a);
// exp(x) * (cos(y) + i sin(y))
	return exp(x)*(cos(y) +  make_cuDoubleComplex(0.,1.)*sin(y));

	}
	
//https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#atomic-functions
__device__ void atomicAdd(cuDoubleComplex* a, cuDoubleComplex b){
  //transform the addresses of real and imag. parts to double pointers
  double *x = (double*)a;
  double *y = x+1;
  //use atomicAdd for double variables
  atomicAdd(x, cuCreal(b));
  atomicAdd(y, cuCimag(b));
}
