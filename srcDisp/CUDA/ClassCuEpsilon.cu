// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

class cu_epsilon{
	public:
	cuDoubleComplex XX,YY,ZZ,XY,YX,ZY,YZ,ZX,XZ;
	cuDoubleComplex XXD,YYD,ZZD,XYD,YXD,ZYD,YZD,ZXD,XZD;
	__device__ __host__ cu_epsilon();
	__device__ __host__ void Set(cuDoubleComplex num);

 };
 
__device__ __host__ void cu_epsilon::Set(cuDoubleComplex num)
{
	XX=YY=ZZ=XY=YX=ZY=YZ=ZX=XZ=num;
	XXD=YYD=ZZD=XYD=YXD=ZYD=YZD=ZXD=XZD=num;
}

__device__ __host__ cu_epsilon::cu_epsilon() // конструктор по умолчанию 
{
	cuDoubleComplex CNull=make_cuDoubleComplex(0.,0.);
	XX=YY=ZZ=XY=YX=ZY=YZ=ZX=XZ=CNull;
	XXD=YYD=ZZD=XYD=YXD=ZYD=YZD=ZXD=XZD=CNull;//производные по частоте 
};

__device__ __host__ cu_epsilon  operator*(double num, cu_epsilon eps) { 
	cu_epsilon res;
	res.XX=num*eps.XX;
	res.YY=num*eps.YY;
	res.ZZ=num*eps.ZZ;
	res.XY=num*eps.XY;
	res.YX=num*eps.YX;
	res.ZY=num*eps.ZY;
	res.YZ=num*eps.YZ;
	res.ZX=num*eps.ZX;
	res.XZ=num*eps.XZ;
	
	res.XXD=num*eps.XXD;
	res.YYD=num*eps.YYD;
	res.ZZD=num*eps.ZZD;
	res.XYD=num*eps.XYD;
	res.YXD=num*eps.YXD;
	res.ZYD=num*eps.ZYD;
	res.YZD=num*eps.YZD;
	res.ZXD=num*eps.ZXD;
	res.XZD=num*eps.XZD;
return res; 
}
__device__ __host__ cu_epsilon  operator*(cu_epsilon eps,double num) { 
	cu_epsilon res;
	res.XX=num*eps.XX;
	res.YY=num*eps.YY;
	res.ZZ=num*eps.ZZ;
	res.XY=num*eps.XY;
	res.YX=num*eps.YX;
	res.ZY=num*eps.ZY;
	res.YZ=num*eps.YZ;
	res.ZX=num*eps.ZX;
	res.XZ=num*eps.XZ;
	
	res.XXD=num*eps.XXD;
	res.YYD=num*eps.YYD;
	res.ZZD=num*eps.ZZD;
	res.XYD=num*eps.XYD;
	res.YXD=num*eps.YXD;
	res.ZYD=num*eps.ZYD;
	res.YZD=num*eps.YZD;
	res.ZXD=num*eps.ZXD;
	res.XZD=num*eps.XZD;
return res; 
}

__device__ __host__ cu_epsilon  operator+(cu_epsilon eps,cu_epsilon eps2) { 
	cu_epsilon res;
	res.XX=eps.XX+eps2.XX;
	res.YY=eps.YY+eps2.YY;
	res.ZZ=eps.ZZ+eps2.ZZ;
	res.XY=eps.XY+eps2.XY;
	res.YX=eps.YX+eps2.YX;
	res.ZY=eps.ZY+eps2.ZY;
	res.YZ=eps.YZ+eps2.YZ;
	res.ZX=eps.ZX+eps2.ZX;
	res.XZ=eps.XZ+eps2.XZ;
	
	res.XXD=eps.XXD+eps2.XXD;
	res.YYD=eps.YYD+eps2.YYD;
	res.ZZD=eps.ZZD+eps2.ZZD;
	res.XYD=eps.XYD+eps2.XYD;
	res.YXD=eps.YXD+eps2.YXD;
	res.ZYD=eps.ZYD+eps2.ZYD;
	res.YZD=eps.YZD+eps2.YZD;
	res.ZXD=eps.ZXD+eps2.ZXD;
	res.XZD=eps.XZD+eps2.XZD;
return res; 
}

  
__device__ __host__  void EpsToRes(double *Res,cu_epsilon eps)
{
	
Res[0]=cuCreal(eps.XX);
Res[0+9]=cuCimag(eps.XX);
Res[1]=cuCreal(eps.YY);
Res[1+9]=cuCimag(eps.YY);
Res[2]=cuCreal(eps.ZZ);
Res[2+9]=cuCimag(eps.ZZ);
Res[3]=cuCreal(eps.XY);
Res[3+9]=cuCimag(eps.XY);
Res[4]=cuCreal(eps.YX);
Res[4+9]=cuCimag(eps.YX);
Res[5]=cuCreal(eps.XZ);
Res[5+9]=cuCimag(eps.XZ);
Res[6]=cuCreal(eps.ZX);
Res[6+9]=cuCimag(eps.ZX);
Res[7]=cuCreal(eps.YZ);
Res[7+9]=cuCimag(eps.YZ);
Res[8]=cuCreal(eps.ZY);
Res[8+9]=cuCimag(eps.ZY);

Res[18+0]=cuCreal(eps.XXD);
Res[18+0+9]=cuCimag(eps.XXD);
Res[18+1]=cuCreal(eps.YYD);
Res[18+1+9]=cuCimag(eps.YYD);
Res[18+2]=cuCreal(eps.ZZD);
Res[18+2+9]=cuCimag(eps.ZZD);
Res[18+3]=cuCreal(eps.XYD);
Res[18+3+9]=cuCimag(eps.XYD);
Res[18+4]=cuCreal(eps.YXD);
Res[18+4+9]=cuCimag(eps.YXD);
Res[18+5]=cuCreal(eps.XZD);
Res[18+5+9]=cuCimag(eps.XZD);
Res[18+6]=cuCreal(eps.ZXD);
Res[18+6+9]=cuCimag(eps.ZXD);
Res[18+7]=cuCreal(eps.YZD);
Res[18+7+9]=cuCimag(eps.YZD);
Res[18+8]=cuCreal(eps.ZYD);
Res[18+8+9]=cuCimag(eps.ZYD);

}
