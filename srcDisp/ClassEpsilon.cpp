// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

#include <complex>

using namespace std;
using namespace std::complex_literals;
#include "ClassEpsilon.hpp"


void epsilon::Set(complex<double> num)
{
	XX=YY=ZZ=XY=YX=ZY=YZ=ZX=XZ=num;
	XXD=YYD=ZZD=XYD=YXD=ZYD=YZD=ZXD=XZD=num;
}

epsilon::epsilon() // конструктор по умолчанию 
{
	complex<double> CNull=complex<double>(0.,0.);
	XX=YY=ZZ=XY=YX=ZY=YZ=ZX=XZ=CNull;
	XXD=YYD=ZZD=XYD=YXD=ZYD=YZD=ZXD=XZD=CNull;//производные по частоте 
};

epsilon  operator*(double num, epsilon eps) { 
	epsilon res;
	res.XX=num*eps.XX;
	res.YY=num*eps.YY;
	res.ZZ=num*eps.ZZ;
	res.XY=num*eps.XY;
	res.YX=num*eps.YX;
	res.ZY=num*eps.ZY;
	res.YZ=num*eps.YZ;
	res.ZX=num*eps.ZX;
	res.XZ=num*eps.XZ;
	
	res.XXD=num*eps.XXD;
	res.YYD=num*eps.YYD;
	res.ZZD=num*eps.ZZD;
	res.XYD=num*eps.XYD;
	res.YXD=num*eps.YXD;
	res.ZYD=num*eps.ZYD;
	res.YZD=num*eps.YZD;
	res.ZXD=num*eps.ZXD;
	res.XZD=num*eps.XZD;
return res; 
}
epsilon  operator*(epsilon eps,double num) { 
	epsilon res;
	res.XX=num*eps.XX;
	res.YY=num*eps.YY;
	res.ZZ=num*eps.ZZ;
	res.XY=num*eps.XY;
	res.YX=num*eps.YX;
	res.ZY=num*eps.ZY;
	res.YZ=num*eps.YZ;
	res.ZX=num*eps.ZX;
	res.XZ=num*eps.XZ;
	
	res.XXD=num*eps.XXD;
	res.YYD=num*eps.YYD;
	res.ZZD=num*eps.ZZD;
	res.XYD=num*eps.XYD;
	res.YXD=num*eps.YXD;
	res.ZYD=num*eps.ZYD;
	res.YZD=num*eps.YZD;
	res.ZXD=num*eps.ZXD;
	res.XZD=num*eps.XZD;
return res; 
}

epsilon  operator+(epsilon eps,epsilon eps2) { 
	epsilon res;
	res.XX=eps.XX+eps2.XX;
	res.YY=eps.YY+eps2.YY;
	res.ZZ=eps.ZZ+eps2.ZZ;
	res.XY=eps.XY+eps2.XY;
	res.YX=eps.YX+eps2.YX;
	res.ZY=eps.ZY+eps2.ZY;
	res.YZ=eps.YZ+eps2.YZ;
	res.ZX=eps.ZX+eps2.ZX;
	res.XZ=eps.XZ+eps2.XZ;
	
	res.XXD=eps.XXD+eps2.XXD;
	res.YYD=eps.YYD+eps2.YYD;
	res.ZZD=eps.ZZD+eps2.ZZD;
	res.XYD=eps.XYD+eps2.XYD;
	res.YXD=eps.YXD+eps2.YXD;
	res.ZYD=eps.ZYD+eps2.ZYD;
	res.YZD=eps.YZD+eps2.YZD;
	res.ZXD=eps.ZXD+eps2.ZXD;
	res.XZD=eps.XZD+eps2.XZD;
return res; 
}
