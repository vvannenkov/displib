//заголовочный файл для функций, доступных из библиотеки

//главная функция. Возвращает Eps. (горячий или холодный) Считает или на GPU, или на CPU по openMP в зависимости от компиляции библиотеки
void Eps(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res);
//делает нормировку функции распределения, сразу записывая её в файл параметров
void Make_Distribution_Norm(double* PartParams,double* IntParams);
//вакуумные эпсилоны
void VacEps(double *Res);
//дисперсионная функция
void Dispersion(double w_real,double w_imag,double kl, double kt, double* Epsilon, double* Res);

//посчитать G-интегралы
void TestG(double Omega,double gamP,double Re_a,double Im_a,double z,int Ng,double *Res);

//напечатать эпсилон методами c++
void PrintEps(double *Epsilon,int Prec);

//однопоточное вычисление тензора
void HotEpsSerial(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res);
//GPU
void HotEpsCUDA(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res);
//по openMP
void HotEpsMP(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res);
//холодная плазма, двигающаяся вдоль магнитного поля
void ColdPlasma(double w_real,double w_imag,double kl, double kt,double* PartParams,double *Res);


//возвращает аналитическую нормировку функции распределения
void Distribution_Norm(double* PartParams,double *Norm);
//возвращает численную нормировку функции распределения
void Distribution_Numerical_Norm(double* PartParams,double* IntParams,double *Norm);
//возвращает интеграл функции распределения  в заданных пределах (для численной нормировки) в минус первой стпени
//однопоточная версия
void Distribution_NumericalSerial_Norm(double* PartParams,double* IntParams,double *Norm);
//возвращает значение функции распределения и её производные
void extDistribution(double* PartParams,double Norm,double PL,double PT,double *Distrib);

//print properties of all CUDA devices 
void AllDevicesProp(void);
//print properties of one CUDA device
void DeviceProp(const int gpu);

//find a solution of dispersion equation using Newton's method
void NewtonsMethod(double w0_real,double w0_imag,double kl, double kt,int NumOfSorts, double** PartParams, double** IntParams, double* w_res,double Precision,int Iter_max);
