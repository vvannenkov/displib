// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

//Общие основные функции

#include <complex>
#include <iomanip>      // std::setprecision
#include <iostream>		// std::cout
#include <chrono>
#include <cassert> // для assert()

using namespace std;
using namespace std::complex_literals;

#include "ClassEpsilon.hpp"
#include "DispLocal.hpp"

//проверка обязательных 
//ваккум
extern "C" void VacEps(double *Res){
	epsilon eps;
	eps.XX=complex<double>(1.,0);
	eps.YY=complex<double>(1.,0);
	eps.ZZ=complex<double>(1.,0);

	EpsToRes(Res,eps);
}


//просуммировать два эпсилона, представленных в виде массива
extern "C" void EpsSumm(double *Eps1,double *Eps2,double* Res)
{
	for(int i=0;i<36;i++)//количество компонент тензора + производные
	{
		Res[i]=Eps1[i]+Eps2[i];
	}
}

void EpsToRes(double *Res,epsilon eps)
{
	
Res[0]=eps.XX.real();
Res[0+9]=eps.XX.imag();
Res[1]=eps.YY.real();
Res[1+9]=eps.YY.imag();
Res[2]=eps.ZZ.real();
Res[2+9]=eps.ZZ.imag();
Res[3]=eps.XY.real();
Res[3+9]=eps.XY.imag();
Res[4]=eps.YX.real();
Res[4+9]=eps.YX.imag();
Res[5]=eps.XZ.real();
Res[5+9]=eps.XZ.imag();
Res[6]=eps.ZX.real();
Res[6+9]=eps.ZX.imag();
Res[7]=eps.YZ.real();
Res[7+9]=eps.YZ.imag();
Res[8]=eps.ZY.real();
Res[8+9]=eps.ZY.imag();

Res[18+0]=eps.XXD.real();
Res[18+0+9]=eps.XXD.imag();
Res[18+1]=eps.YYD.real();
Res[18+1+9]=eps.YYD.imag();
Res[18+2]=eps.ZZD.real();
Res[18+2+9]=eps.ZZD.imag();
Res[18+3]=eps.XYD.real();
Res[18+3+9]=eps.XYD.imag();
Res[18+4]=eps.YXD.real();
Res[18+4+9]=eps.YXD.imag();
Res[18+5]=eps.XZD.real();
Res[18+5+9]=eps.XZD.imag();
Res[18+6]=eps.ZXD.real();
Res[18+6+9]=eps.ZXD.imag();
Res[18+7]=eps.YZD.real();
Res[18+7+9]=eps.YZD.imag();
Res[18+8]=eps.ZYD.real();
Res[18+8+9]=eps.ZYD.imag();

}

void ResToEps(double *Res,epsilon &eps)
{
	eps.XX=complex<double>(Res[0],Res[0+9]);
	eps.YY=complex<double>(Res[1],Res[1+9]);
	eps.ZZ=complex<double>(Res[2],Res[2+9]);
	eps.XY=complex<double>(Res[3],Res[3+9]);
	eps.YX=complex<double>(Res[4],Res[4+9]);
	eps.XZ=complex<double>(Res[5],Res[5+9]);
	eps.ZX=complex<double>(Res[6],Res[6+9]);
	eps.YZ=complex<double>(Res[7],Res[7+9]);
	eps.ZY=complex<double>(Res[8],Res[8+9]);


	eps.XXD=complex<double>(Res[18+0],Res[18+0+9]);
	eps.YYD=complex<double>(Res[18+1],Res[18+1+9]);
	eps.ZZD=complex<double>(Res[18+2],Res[18+2+9]);
	eps.XYD=complex<double>(Res[18+3],Res[18+3+9]);
	eps.YXD=complex<double>(Res[18+4],Res[18+4+9]);
	eps.XZD=complex<double>(Res[18+5],Res[18+5+9]);
	eps.ZXD=complex<double>(Res[18+6],Res[18+6+9]);
	eps.YZD=complex<double>(Res[18+7],Res[18+7+9]);
	eps.ZYD=complex<double>(Res[18+8],Res[18+8+9]);



}


extern "C" void Dispersion(double w_real,double w_imag,double kl, double kt, double* Epsilon, double* Res)
{
	complex<double> FUNC;
	complex<double> DFUNC;

	complex<double> w=complex<double>(w_real,w_imag);
	epsilon eps;
	eps.XX=complex<double>(Epsilon[0],Epsilon[0+9]);
	eps.YY=complex<double>(Epsilon[1],Epsilon[1+9]);
	eps.ZZ=complex<double>(Epsilon[2],Epsilon[2+9]);
	eps.XY=complex<double>(Epsilon[3],Epsilon[3+9]);
	eps.YX=complex<double>(Epsilon[4],Epsilon[4+9]);
	eps.XZ=complex<double>(Epsilon[5],Epsilon[5+9]);
	eps.ZX=complex<double>(Epsilon[6],Epsilon[6+9]);
	eps.YZ=complex<double>(Epsilon[7],Epsilon[7+9]);
	eps.ZY=complex<double>(Epsilon[8],Epsilon[8+9]);


	eps.XXD=complex<double>(Epsilon[18+0],Epsilon[18+0+9]);
	eps.YYD=complex<double>(Epsilon[18+1],Epsilon[18+1+9]);
	eps.ZZD=complex<double>(Epsilon[18+2],Epsilon[18+2+9]);
	eps.XYD=complex<double>(Epsilon[18+3],Epsilon[18+3+9]);
	eps.YXD=complex<double>(Epsilon[18+4],Epsilon[18+4+9]);
	eps.XZD=complex<double>(Epsilon[18+5],Epsilon[18+5+9]);
	eps.ZXD=complex<double>(Epsilon[18+6],Epsilon[18+6+9]);
	eps.YZD=complex<double>(Epsilon[18+7],Epsilon[18+7+9]);
	eps.ZYD=complex<double>(Epsilon[18+8],Epsilon[18+8+9]);

///! Здесь учтено что epsYZ=-epsZY и epsXY=-epsYX
 FUNC=(eps.ZZ*kl*kl*kl*kl*w*w+double(2.)*eps.XZ*kl*kl*kl*kt*w*w+eps.XX*kl*kl*kt*kt*w*w+eps.ZZ*kl*kl*kt*kt*w*w+double(2.)*eps.XZ*kl*kt*kt*kt*w*w+eps.XX*kt*kt*kt*kt*w*w+eps.XZ*eps.XZ*kl*kl*w*w*w*w-eps.YZ*eps.YZ*kl*kl*w*w*w*w-eps.XX*eps.ZZ*kl*kl*w*w*w*w-eps.YY*eps.ZZ*kl*kl*w*w*w*w-double(2.)*eps.XZ*eps.YY*kl*kt*w*w*w*w+double(2.)*eps.XY*eps.YZ*kl*kt*w*w*w*w-eps.XY*eps.XY*kt*kt*w*w*w*w+eps.XZ*eps.XZ*kt*kt*w*w*w*w-eps.XX*eps.YY*kt*kt*w*w*w*w-eps.XX*eps.ZZ*kt*kt*w*w*w*w-eps.XZ*eps.XZ*eps.YY*w*w*w*w*w*w+double(2.)*eps.XY*eps.XZ*eps.YZ*w*w*w*w*w*w+eps.XX*eps.YZ*eps.YZ*w*w*w*w*w*w+eps.XY*eps.XY*eps.ZZ*w*w*w*w*w*w+eps.XX*eps.YY*eps.ZZ*w*w*w*w*w*w);

 DFUNC=  (double(2.)*kl*kl*kt*kt*w*eps.XX+double(2.)*kt*kt*kt*kt*w*eps.XX-double(4.)*kt*kt*w*w*w*eps.XY*eps.XY+double(4.)*kl*kl*kl*kt*w*eps.XZ+double(4.)*kl*kt*kt*kt*w*eps.XZ+double(4.)*kl*kl*w*w*w*eps.XZ*eps.XZ+double(4.)*kt*kt*w*w*w*eps.XZ*eps.XZ-double(4.)*kt*kt*w*w*w*eps.XX*eps.YY-double(8.)*kl*kt*w*w*w*eps.XZ*eps.YY-double(6.)*w*w*w*w*w*eps.XZ*eps.XZ*eps.YY+double(8.)*kl*kt*w*w*w*eps.XY*eps.YZ+double(12.)*w*w*w*w*w*eps.XY*eps.XZ*eps.YZ-double(4.)*kl*kl*w*w*w*eps.YZ*eps.YZ+double(6.)*w*w*w*w*w*eps.XX*eps.YZ*eps.YZ+double(2.)*kl*kl*kl*kl*w*eps.ZZ+double(2.)*kl*kl*kt*kt*w*eps.ZZ-double(4.)*kl*kl*w*w*w*eps.XX*eps.ZZ-double(4.)*kt*kt*w*w*w*eps.XX*eps.ZZ+double(6.)*w*w*w*w*w*eps.XY*eps.XY*eps.ZZ-double(4.)*kl*kl*w*w*w*eps.YY*eps.ZZ+double(6.)*w*w*w*w*w*eps.XX*eps.YY*eps.ZZ+kl*kl*kt*kt*w*w*(eps.XXD)+kt*kt*kt*kt*w*w*(eps.XXD)-kt*kt*w*w*w*w*eps.YY*(eps.XXD)+w*w*w*w*w*w*eps.YZ*eps.YZ*(eps.XXD)-kl*kl*w*w*w*w*eps.ZZ*(eps.XXD)-kt*kt*w*w*w*w*eps.ZZ*(eps.XXD)+w*w*w*w*w*w*eps.YY*eps.ZZ*(eps.XXD)-double(2.)*kt*kt*w*w*w*w*eps.XY*(eps.XYD)+double(2.)*kl*kt*w*w*w*w*eps.YZ*(eps.XYD)+double(2.)*w*w*w*w*w*w*eps.XZ*eps.YZ*(eps.XYD)+double(2.)*w*w*w*w*w*w*eps.XY*eps.ZZ*(eps.XYD)+double(2.)*kl*kl*kl*kt*w*w*(eps.XZD)+double(2.)*kl*kt*kt*kt*w*w*(eps.XZD)+double(2.)*kl*kl*w*w*w*w*eps.XZ*(eps.XZD)+double(2.)*kt*kt*w*w*w*w*eps.XZ*(eps.XZD)-double(2.)*kl*kt*w*w*w*w*eps.YY*(eps.XZD)-double(2.)*w*w*w*w*w*w*eps.XZ*eps.YY*(eps.XZD)+double(2.)*w*w*w*w*w*w*eps.XY*eps.YZ*(eps.XZD)-kt*kt*w*w*w*w*eps.XX*(eps.YYD)-double(2.)*kl*kt*w*w*w*w*eps.XZ*(eps.YYD)-w*w*w*w*w*w*eps.XZ*eps.XZ*(eps.YYD)-kl*kl*w*w*w*w*eps.ZZ*(eps.YYD)+w*w*w*w*w*w*eps.XX*eps.ZZ*(eps.YYD)+double(2.)*kl*kt*w*w*w*w*eps.XY*(eps.YZD)+double(2.)*w*w*w*w*w*w*eps.XY*eps.XZ*(eps.YZD)-double(2.)*kl*kl*w*w*w*w*eps.YZ*(eps.YZD)+double(2.)*w*w*w*w*w*w*eps.XX*eps.YZ*(eps.YZD)+kl*kl*kl*kl*w*w*(eps.ZZD)+kl*kl*kt*kt*w*w*(eps.ZZD)-kl*kl*w*w*w*w*eps.XX*(eps.ZZD)-kt*kt*w*w*w*w*eps.XX*(eps.ZZD)+w*w*w*w*w*w*eps.XY*eps.XY*(eps.ZZD)-kl*kl*w*w*w*w*eps.YY*(eps.ZZD)+w*w*w*w*w*w*eps.XX*eps.YY*(eps.ZZD));
Res[0]=FUNC.real();
Res[1]=FUNC.imag();
Res[2]=DFUNC.real();
Res[3]=DFUNC.imag();

}


extern "C" void PrintEps(double *Epsilon,int Prec)
{
	
cout <<"Epsilon"<<endl;
cout <<"Exx="<<Epsilon[0]<<"  "<<Epsilon[9]<<"i"<<" Exy="<<Epsilon[3]<<"  "<<Epsilon[12]<<"i"<<" Exz="<<Epsilon[5]<<"  "<<Epsilon[14]<<"i"<<endl;
cout << setprecision(Prec)<< "Eyx="<<Epsilon[4]<<"  "<<Epsilon[4+9]<<"i"<<" Eyy="<<Epsilon[1]<<"  "<<Epsilon[1+9]<<"i"<<" Eyz="<<Epsilon[7]<<"  "<<Epsilon[7+9]<<"i"<<endl;
cout << setprecision(Prec)<< "Ezx="<<Epsilon[6]<<"  "<<Epsilon[6+9]<<"i"<<" Ezy="<<Epsilon[8]<<"  "<<Epsilon[8+9]<<"i"<<" Ezz="<<Epsilon[2]<<"  "<<Epsilon[2+9]<<"i"<<endl;
cout <<"Derivatives DE/Dw"<<endl;
cout << setprecision(Prec)<< "DExx="<<Epsilon[18+0]<<"  "<<Epsilon[18+0+9]<<"i"<<" DExy="<<Epsilon[18+3]<<"  "<<Epsilon[18+3+9]<<"i"<<" DExz="<<Epsilon[18+5]<<"  "<<Epsilon[18+5+9]<<"i"<<endl;
cout << setprecision(Prec)<< "DEyx="<<Epsilon[18+4]<<"  "<<Epsilon[18+4+9]<<"i"<<" DEyy="<<Epsilon[18+1]<<"  "<<Epsilon[18+1+9]<<"i"<<" DEyz="<<Epsilon[18+7]<<"  "<<Epsilon[18+7+9]<<"i"<<endl;
cout << setprecision(Prec)<< "DEzx="<<Epsilon[18+6]<<"  "<<Epsilon[18+6+9]<<"i"<<" DEzy="<<Epsilon[18+8]<<"  "<<Epsilon[18+8+9]<<"i"<<" DEzz="<<Epsilon[18+2]<<"  "<<Epsilon[18+2+9]<<"i"<<endl;
	
}

//посчитать G-интегралы
extern "C" void TestG(double Omega,double gamP,double Re_a,double Im_a,double z,int Ng,double *Res)
{
	
	chrono::steady_clock::time_point begin;
	chrono::steady_clock::time_point end;
	
	complex<double> g0,g1,g2,Dg0,Dg1,Dg2;
	complex<double> a=complex<double>(Re_a,Im_a);

	CalcAllGint(Omega,gamP,a, z,Ng,g0,g1,g2,Dg0,Dg1,Dg2);

	Res[0]=g0.real();
	Res[1]=g0.imag();
	Res[2]=g1.real();
	Res[3]=g1.imag();
	Res[4]=g2.real();
	Res[5]=g2.imag();
	Res[6]=Dg0.real();
	Res[7]=Dg0.imag();
	Res[8]=Dg1.real();
	Res[9]=Dg1.imag();
	Res[10]=Dg2.real();
	Res[11]=Dg2.imag();


}
//проверка параметров интегрирования
void CheckIntParams(double *IntParams)
{
	assert(IntParams[2] > 0 &&  "Wrong number of segments for pl integral (Nl)"); 
	assert(IntParams[5] > 0 && "Wrong number of segments for pt integral (Nt)"); 
	assert(IntParams[6] > 0 && "Wrong number of segments for G integrals (Ng)"); 
	assert(IntParams[3] >= 0 && "Min_pt have to be >=0 (cylindrical coordinates)"); 
}
//проверка обязательных параметров частиц
void CheckPartParams(double *PartParams)
{
	//temporary
	assert((PartParams[2]) == 1 &&  "Particles charge have to be = 1 (only electrons)"); 
	assert((PartParams[3]) == 1 &&  "Particles mass have to be =1 (only electrons)");

	assert(PartParams[1] > 0 &&  "Particles density have to be >0"); 
	assert(PartParams[2] > 0 &&  "Particles mass have to be >0"); 
	assert(PartParams[6] > 0 &&  "Distribution norm have to be >0"); 

}
