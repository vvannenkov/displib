// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

#pragma once

class epsilon{
	public:
	complex<double> XX,YY,ZZ,XY,YX,ZY,YZ,ZX,XZ;
	complex<double> XXD,YYD,ZZD,XYD,YXD,ZYD,YZD,ZXD,XZD;
	epsilon();
	void Set(complex<double> num);

 };
 
extern epsilon  operator*(double num, epsilon eps);
extern epsilon  operator*(epsilon eps,double num);
extern epsilon  operator+(epsilon eps,epsilon eps2); 
