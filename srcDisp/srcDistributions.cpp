// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

#include <cmath>
#include <omp.h>//OpenMP
#include <stdio.h>
#include <chrono>
#include <iostream>
#include <cassert>

//функции распределения
using namespace std;

//Нормировку функции распределения считаем внешним параметром (вычисленным численно или аналитически)


#include "./srcDistributions/distrAnysMaxwell.hpp"
#include "./srcDistributions/distrMoon.hpp"

//возвращает значение функции распределения
void Distribution(double* PartParams,double Norm,double PL,double PT,double *Distrib)
{
	int PartType=PartParams[0];//тип распределения

	#include "./srcDistributions/distrFunc.hpp"

}
//возвращает число параметров распределения (необходимо для GPU)
int Distribution_ParamNum(double* PartParams)
{
	int PartType=PartParams[0];//тип распределения
	switch(PartType)
	{
		//анизатропное распределение Максвелла
		case(1)://Maxwell
		{
			return 10;
		}
		break;	
		//ужатое пробкой распределение
		case(2)://Moon
		{
			return 10;
		}
		break;	
	}
	return -1;
		
}

//возвращает аналитическую нормировку функции распределения
extern "C" void Distribution_Norm(double* PartParams,double &Norm)
{
	int PartType=PartParams[0];//тип распределения

	switch(PartType)
	{
		//анизатропное распределение Максвелла
		case(1)://Maxwell
		{
			distr_Norm_AnisotropicMaxwell(PartParams,Norm);
		}
		break;	
		
		default:
		{
			Norm=-1;//нет аналитической нормировки
		}
		break;	
	}
}


extern "C" void extDistribution(double* PartParams,double Norm,double PL,double PT,double *Distrib)
{
	Distribution(PartParams,Norm,PL,PT,Distrib);
}

//возвращает интеграл функции распределения  в заданных пределах (для численной нормировки) в минус первой стпени
//однопоточная версия
extern "C" void Distribution_NumericalSerial_Norm(double* PartParams,double* IntParams,double &Norm)
{

	double Min_pl=IntParams[0];//левый предел продольного импульса
	double Max_pl=IntParams[1];//правый предел продольного импульса
	size_t Nl=IntParams[2];//количество отрезков интегрирования
	double Min_pt=IntParams[3];//левый предел поперечного импульса
	double Max_pt=IntParams[4];//правый предел поперечного импульса
	size_t Nt=IntParams[5];//количество отрезков интегрирования

	double dpl=(Max_pl-Min_pl)/double(Nl);//шаг по продольному импульсу
	double dpt=(Max_pt-Min_pt)/double(Nt);//шаг по поперечному импульсу
	
	double PL,PT;
	double Int=0;
	double Res[3];
	
	chrono::steady_clock::time_point begin;
	chrono::steady_clock::time_point end;
	begin = chrono::steady_clock::now();
	
	for(size_t il=0;il<Nl;++il)
	{
		PL=Min_pl+(double(il)+0.5)*dpl;
		for(size_t it=0;it<Nt;++it)
		{
			PT=Min_pt+(double(it)+0.5)*dpt;
			Distribution(PartParams,1.,PL,PT,Res);	
			Int+=Res[0]*PT;
		}
	}
	Int*=dpt*dpl*double(2.)*M_PI;
	Norm=(double)1./Int;

}



//возвращает интеграл функции распределения  в заданных пределах (для численной нормировки) в минус первой стпени
extern "C"  void Distribution_Numerical_Norm(double* PartParams,double* IntParams,double &Norm)
{

//IntParamsPlasma=[Min_pl,Max_pl,Nl,Min_pt,Max_pt,Nt,Ng]

	double Min_pl=IntParams[0];//левый предел продольного импульса
	double Max_pl=IntParams[1];//правый предел продольного импульса
	size_t Nl=IntParams[2];//количество отрезков интегрирования
	double Min_pt=IntParams[3];//левый предел поперечного импульса
	double Max_pt=IntParams[4];//правый предел поперечного импульса
	size_t Nt=IntParams[5];//количество отрезков интегрирования

	double dpl=(Max_pl-Min_pl)/double(Nl);//шаг по продольному импульсу
	double dpt=(Max_pt-Min_pt)/double(Nt);//шаг по поперечному импульсу
	
	double PL,PT;
	double Int=0;
	
	//параметры интегрирования
	//IntParams[левый предел, правый предел, число отрезков интегрирования]

	//int ThreadNum=IntParams[7];//сколько потоков использовать
	
	//if(ThreadNum==0)//use all threads
	
	//use all threads
	int ThreadNum=omp_get_max_threads();

	//assert(ThreadNum >= 1 && "Wrong amount of MP threads"); 

	#pragma omp parallel num_threads(ThreadNum) reduction(+:Int)
	{
		double Res[3];
		int tid = omp_get_thread_num();
		Int=0;
		double PL,PT;
		for (size_t index = tid; index < Nl*Nt; index+=ThreadNum) {
			PL =Min_pl+ (index / Nt+double(0.5))*dpl;
			PT =Min_pt+(index % Nl+double(0.5))*dpt;

			Distribution(PartParams,1.,PL,PT,Res);
			Int+=Res[0]*PT;	
		}
	}
	Int*=dpt*dpl*double(2.)*M_PI;
	Norm=(double)1./Int;
}

//делает нормировку функции распределения, сразу записывая её в файл параметров
extern "C" void Make_Distribution_Norm(double* PartParams,double* IntParams)
{
	double Norm=0;
	//попытка сделать аналитическую нормировку
	Distribution_Norm(PartParams,Norm);
	//если не удалось, то запуск численной
	if(Norm<0)
		Distribution_Numerical_Norm(PartParams,IntParams,Norm);
		
	PartParams[6]=Norm;
}
