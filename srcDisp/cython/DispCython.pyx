#Copyright © 2021 Vladimir Annenkov. All rights reserved.
#Licensed under the Apache License, Version 2.0


from libc.stdlib cimport *

cdef extern from "../include/Disp.h":
	void ColdPlasma(double w_real,double w_imag,double kl, double kt,double* PartParams,double *Res); 
	void Dispersion(double w_real,double w_imag,double kl, double kt, double* Epsilon, double* Res);
	void VacEps(double *Res);
	#возвращает аналитическую нормировку функции распределения
	void Distribution_Norm(double* PartParams,double *Norm);
	#возвращает численную нормировку функции распределения
	void Distribution_Numerical_Norm(double* PartParams,double* IntParams,double *Norm);
	void Distribution_NumericalSerial_Norm(double* PartParams,double* IntParams,double *Norm);
	#возвращает значение функции распределения и её производные
	void extDistribution(double* PartParams,double Norm,double PL,double PT,double *Distrib);
	void TestG(double Omega,double gamP,double Re_a,double Im_a,double z,int Ng,double *Res);
	#главная функция. Возвращает Eps. Считает или на GPU, или на CPU по openMP в зависимости от компиляции библиотеки
	void Eps(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res);
	#однопоточное вычисление тензора
	void HotEpsSerial(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res);
	#GPU
	void HotEpsCUDA(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res);
	#по openMP
	void HotEpsMP(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res);
	#print properties of all CUDA devices 
	void AllDevicesProp();
	#print properties of one CUDA device
	void DeviceProp(const int gpu);
	#делает нормировку функции распределения, сразу записывая её в файл параметров
	void Make_Distribution_Norm(double* PartParams,double* IntParams);
	#find a solution of dispersion equation using Newton's method
	void NewtonsMethod(double w0_real,double w0_imag,double kl, double kt,int NumOfSorts, double** PartParams, double** IntParams, double* w_res,double Precision,int Iter_max);

import numpy as np
import pprint
from math import fabs


#find a solution of dispersion equation using Newton's method
def pyNewtonsMethod(w_real,w_imag,kl, kt,PartParams,IntParams, Precision,Iter_max):
	cdef double cw_real=w_real
	cdef double cw_imag=w_imag
	cdef double ckl=kl
	cdef double ckt=kt
	cdef double cPrecision=Precision
	cdef int cIter_max=Iter_max
	cdef double *cRes
	cdef double **cPartParams
	cdef double **cIntParams

	cdef int cNumOfSorts
	cRes = <double *>malloc(2*sizeof(double))
	cNumOfSorts =int(len(PartParams))
	cPartParams = <double **>malloc(cNumOfSorts*sizeof(double*))
	cIntParams = <double **>malloc(cNumOfSorts*sizeof(double*))

	for i in range(cNumOfSorts):
		cIntParams[i]= <double *>malloc(8*sizeof(double))
		for k in range(0,8):
			if(len(IntParams[i])>0):
				cIntParams[i][k]=IntParams[i][k]
		cPartParams[i] = <double *>malloc(len(PartParams[i])*sizeof(double))
		for j in range(0,len(PartParams[i])):
			cPartParams[i][j]=PartParams[i][j]
		
	NewtonsMethod(cw_real,cw_imag,ckl,ckt,cNumOfSorts,cPartParams,cIntParams,cRes,cPrecision,cIter_max)
	npRes=np.empty(2,dtype=np.float64)
	for i in range(2):
		npRes[i] = cRes[i]
	for i in range(cNumOfSorts):
		free(cPartParams[i])
		free(cIntParams[i])
	free(cPartParams)
	free(cIntParams)
	free(cRes)
	w=complex(npRes[0],npRes[1])
	return w

def pyMake_Distribution_Norm(PartParams,IntParams):
	cdef double *cPartParams
	cdef double *cIntParams
	cdef int PartNum
	cdef int IntParamNum
	Res = <double *>malloc(2*9*2*sizeof(double))
	
	PartNum =len(PartParams)
	cPartParams = <double *>malloc(PartNum*sizeof(double))
	for i in range(PartNum):
		cPartParams[i] = (PartParams[i])
	
	IntParamNum=len(IntParams)
	cIntParams = <double *>malloc(IntParamNum*sizeof(double))
	for i in range(IntParamNum):
		cIntParams[i] = (IntParams[i])
	
	Make_Distribution_Norm(cPartParams,cIntParams)
	PartParams[6]=cPartParams[6]
	free(cPartParams)
	free(cIntParams)
		
def pyHotEps_serial(w_real,w_imag,kl, kt,PartParams,IntParams):
	cdef double *Res
	cdef double *cPartParams
	cdef double *cIntParams
	cdef int PartNum
	cdef int IntParamNum
	Res = <double *>malloc(2*9*2*sizeof(double))
	
	PartNum =len(PartParams)
	cPartParams = <double *>malloc(PartNum*sizeof(double))
	for i in range(PartNum):
		cPartParams[i] = (PartParams[i])
	
	IntParamNum=len(IntParams)
	cIntParams = <double *>malloc(IntParamNum*sizeof(double))
	for i in range(IntParamNum):
		cIntParams[i] = (IntParams[i])
		
	HotEpsSerial(w_real,w_imag,kl,kt,cPartParams,cIntParams,Res)

	npRes=np.empty(2*9*2,dtype=np.float64)
	for i in range(len(npRes)):
		npRes[i] = Res[i]
	free(cPartParams)
	free(cIntParams)
	free(Res)
	return npRes
	
def pyEps(w_real,w_imag,kl, kt,PartParams,IntParams):
	cdef double *Res
	cdef double *cPartParams
	cdef double *cIntParams
	cdef int PartNum
	cdef int IntParamNum
	Res = <double *>malloc(2*9*2*sizeof(double))
	
	PartNum =len(PartParams)
	cPartParams = <double *>malloc(PartNum*sizeof(double))
	for i in range(PartNum):
		cPartParams[i] = (PartParams[i])
	
	IntParamNum=len(IntParams)
	cIntParams = <double *>malloc(IntParamNum*sizeof(double))
	for i in range(IntParamNum):
		cIntParams[i] = (IntParams[i])
		
	#HotPlasma(w_real,w_imag,kl,kt,cPartParams,cIntParams,Res)
	Eps(w_real,w_imag,kl,kt,cPartParams,cIntParams,Res)

	npRes=np.empty(2*9*2,dtype=np.float64)
	for i in range(len(npRes)):
		npRes[i] = Res[i]
	free(cPartParams)
	free(cIntParams)
	free(Res)
	return npRes
	
def pyHotEpsMP(w_real,w_imag,kl, kt,PartParams,IntParams):
	cdef double *Res
	cdef double *cPartParams
	cdef double *cIntParams
	cdef int PartNum
	cdef int IntParamNum
	Res = <double *>malloc(2*9*2*sizeof(double))
	
	PartNum =len(PartParams)
	cPartParams = <double *>malloc(PartNum*sizeof(double))
	for i in range(PartNum):
		cPartParams[i] = (PartParams[i])
	
	IntParamNum=len(IntParams)
	cIntParams = <double *>malloc(IntParamNum*sizeof(double))
	for i in range(IntParamNum):
		cIntParams[i] = (IntParams[i])
		
	#HotPlasma(w_real,w_imag,kl,kt,cPartParams,cIntParams,Res)
	HotEpsMP(w_real,w_imag,kl,kt,cPartParams,cIntParams,Res)

	npRes=np.empty(2*9*2,dtype=np.float64)
	for i in range(len(npRes)):
		npRes[i] = Res[i]
	free(cPartParams)
	free(cIntParams)
	free(Res)
	return npRes


#функция распределения и её производные в точки PL,PT
def pyDistribution(PartParams,Norm,PL,PT):
	cdef double *cRes
	cRes = <double *>malloc(3*sizeof(double))
	cdef cPL,cPT
	cPL=PL
	cPT=PT
	cdef double *cPartParams
	cdef int PartNum
	PartNum =len(PartParams)
	cPartParams = <double *>malloc(PartNum*sizeof(double))
	for i in range(PartNum):
		cPartParams[i] = (PartParams[i])
	cdef double cNorm
	cNorm=Norm
	extDistribution(cPartParams,Norm,cPL,cPT,cRes)
	
	Distrib=cRes[0]
	DlDistrib=cRes[1]#производная по PL
	DtDistrib=cRes[2]#производная по PT
	
	free(cRes)
	free(cPartParams)
	return Distrib,DlDistrib,DtDistrib
	
def pyDistribution_Norm(PartParams):
	cdef double *cPartParams
	cdef double cNorm
	cdef int PartNum
	PartNum =len(PartParams)
	cPartParams = <double *>malloc(PartNum*sizeof(double))

	for i in range(PartNum):
		cPartParams[i] = (PartParams[i])
	Distribution_Norm(cPartParams,&cNorm)
	Norm=cNorm
	free(cPartParams)
	return Norm
	
#возвращает интеграл от функции распределения в минус первой степени	
def pyDistribution_NumericalSerial_Norm(PartParams,IntParams):
	cdef double *cPartParams
	cdef double *cIntParams
	cdef double cNorm
	cdef int PartNum
	PartNum =len(PartParams)
	cPartParams = <double *>malloc(PartNum*sizeof(double))
	for i in range(PartNum):
		cPartParams[i] = (PartParams[i])
		
	cdef int IntParamNum
	IntParamNum=len(IntParams)	
	cIntParams = <double *>malloc(IntParamNum*sizeof(double))
	for i in range(IntParamNum):
		cIntParams[i] = (IntParams[i])
		
	Distribution_NumericalSerial_Norm(cPartParams,cIntParams,&cNorm)
	Norm=cNorm
	free(cPartParams)
	free(cIntParams)
	return Norm
	
#возвращает интеграл от функции распределения в минус первой степени	
def pyDistribution_Numerical_Norm(PartParams,IntParams):
	cdef double *cPartParams
	cdef double *cIntParams
	cdef double cNorm
	cdef int PartNum
	PartNum =len(PartParams)
	cPartParams = <double *>malloc(PartNum*sizeof(double))
	for i in range(PartNum):
		cPartParams[i] = (PartParams[i])
	cdef int IntParamNum
	IntParamNum=len(IntParams)	
	cIntParams = <double *>malloc(IntParamNum*sizeof(double))
	for i in range(IntParamNum):
		cIntParams[i] = (IntParams[i])
		
	Distribution_Numerical_Norm(cPartParams,cIntParams,&cNorm)
	Norm=cNorm
	free(cPartParams)
	free(cIntParams)
	return Norm

#посчитать G-интегралы в конкретной точке
def pyTestG(Omega,gamma,Re_a,Im_a,z,Ng):
	cdef double cOmega=Omega
	cdef double cgamma=gamma
	cdef double cRe_a=Re_a
	cdef double cIm_a=Im_a
	cdef double cz=z
	cdef int cNg=Ng
	cdef double *cRes
	cRes = <double *>malloc(12*sizeof(double))
	npRes=np.empty(12,dtype=np.float64)
	
	TestG(cOmega,cgamma,cRe_a,cIm_a,cz,cNg,cRes)
	
	for i in range(12):
		npRes[i] = (cRes[i])
	free(cRes)
	return npRes
	
	

	
	
	
def pyVacEps():
	cdef double *cRes
	cRes = <double *>malloc(2*9*2*sizeof(double))
	VacEps(cRes)
	npRes=np.empty(2*9*2,dtype=np.float64)
	for i in range(len(npRes)):
		npRes[i] = cRes[i]
	free(cRes)
	return npRes
	

def pyPrintEps(Eps,prec):
	def fr(Num,prec):
		sign=''
		if Num<0:
			sign='-'
		else:
			sign='+'
		return sign+format(fabs(Num), '.'+str(prec)+'f')
	print('Epsilons')
	print('Exx='+fr(Eps[0],prec)+fr(Eps[0+9],prec)+'i'+' Exy='+fr(Eps[3],prec)+fr(Eps[3+9],prec)+'i'+' Exz='+fr(Eps[5],prec)+fr(Eps[5+9],prec)+'i')
	print('Eyx='+fr(Eps[4],prec)+fr(Eps[4+9],prec)+'i'+' Eyy='+fr(Eps[1],prec)+fr(Eps[1+9],prec)+'i'+' Eyz='+fr(Eps[7],prec)+fr(Eps[7+9],prec)+'i')
	print('Ezx='+fr(Eps[6],prec)+fr(Eps[6+9],prec)+'i'+' Ezy='+fr(Eps[8],prec)+fr(Eps[8+9],prec)+'i'+' Ezz='+fr(Eps[2],prec)+fr(Eps[2+9],prec)+'i')
	print('Derivatives DE/Dw')
	print('DExx='+fr(Eps[18+0],prec)+fr(Eps[18+0+9],prec)+'i'+' DExy='+fr(Eps[18+3],prec)+fr(Eps[18+3+9],prec)+'i'+' DExz='+fr(Eps[18+5],prec)+fr(Eps[18+5+9],prec)+'i')
	print('DEyx='+fr(Eps[18+4],prec)+fr(Eps[18+4+9],prec)+'i'+' DEyy='+fr(Eps[18+1],prec)+fr(Eps[18+1+9],prec)+'i'+' DEyz='+fr(Eps[18+7],prec)+fr(Eps[18+7+9],prec)+'i')
	print('DEzx='+fr(Eps[18+6],prec)+fr(Eps[18+6+9],prec)+'i'+' DEzy='+fr(Eps[18+8],prec)+fr(Eps[18+8+9],prec)+'i'+' DEzz='+fr(Eps[18+2],prec)+fr(Eps[18+2+9],prec)+'i')


#холодная движущаяся вдоль поля компонента
def pyColdEps(w_real,w_imag,kl, kt,PartParams):
	cdef double *Res
	cdef double *cPartParams
	cdef double *cDevParams
	cdef int PartNum
	Res = <double *>malloc(2*9*2*sizeof(double))
	PartNum =len(PartParams)
	cPartParams = <double *>malloc(PartNum*sizeof(double))

	for i in range(PartNum):
		cPartParams[i] = (PartParams[i])
		
	ColdPlasma(w_real,w_imag,kl,kt,cPartParams,Res)

	npRes=np.empty(2*9*2,dtype=np.float64)
	for i in range(len(npRes)):
		#print(i,Res[i])
		npRes[i] = Res[i]
	free(cPartParams)
	free(Res)
	return npRes

#вычисляет дисперсионное уравнение
#возвращает numpy массив [D.real,D.imag,dD/dw.real,dD/dw.imag]
def pyDispersion(w_real,w_imag,kl, kt, Epsilon):
	cdef double *Res
	Res = <double *>malloc(4*sizeof(double))
	cdef double *cEpsilon
	cEpsilon = <double *>malloc(2*9*2*sizeof(double))
	
	for i in range(len(Epsilon)):
		cEpsilon[i] = Epsilon[i]
	Dispersion(w_real,w_imag,kl, kt,cEpsilon,Res)
	
	npRes=np.empty(4,dtype=np.float64)
	for i in range(len(npRes)):
		npRes[i] = Res[i]
	free(cEpsilon)
	free(Res)
	return npRes

def PrintAllCUDAdevices():
	AllDevicesProp()
def PrintCUDAdeviceProp(gpu):
	cdef int cgpu
	cgpu=gpu
	DeviceProp(cgpu)

#возвращает значение дисперсионного уравнения
def FUNC(w0,args):
	kl=args[0]
	kt=args[1]
	PartParams=args[2]
	IntParams=args[3]
	w=complex(w0[0],w0[1])
	AllEps=pyVacEps()
	p=0
	for Param in PartParams:
		IntParam=IntParams[p]
		Eps=pyEps(w.real,w.imag,kl, kt,Param,IntParam)
		AllEps+=Eps
		p+=1

	Dispersion=pyDispersion(w.real,w.imag,kl, kt, AllEps)
	F=complex(Dispersion[0],Dispersion[1])
	return F.real,F.imag
	


