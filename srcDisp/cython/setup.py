from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

disp_extension = Extension(
    name="DispCython",
    sources=["./srcDisp/cython/DispCython.pyx"],
    libraries=["Disp"],
    library_dirs=["/opt/DispLib/lib/"],
    include_dirs=["/opt/DispLib/include/"],
    runtime_library_dirs=["/opt/DispLib/lib/"],
    extra_compile_args=['-O3', '-march=native']
)

setup(
    name="DispCython",
    ext_modules=cythonize(disp_extension, language_level = "3")
)
