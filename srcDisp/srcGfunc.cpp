// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

#include <complex>

using namespace std;
using namespace std::complex_literals;

#include "Gintegrals.hpp"

void  CalcAllGint(double Omega,double gamP,complex<double> a,double z,int Ng,complex<double> &g0,complex<double> &g1, complex<double> &g2,complex<double> &Dg0,complex<double> &Dg1, complex<double> &Dg2)
{

	const complex<double> I=complex<double>(0.,1.0);//мнимая единица
	const complex<double> CNull=complex<double>(0.,0.);//комплексный ноль

	double phi=0,hG=double(2.)*double(M_PI)/double(Ng);
	complex<double> g0t=CNull,g1t=CNull, g2t=CNull;
	double SIN, COS,Bessel;
	complex<double> EXP;

	complex<double> Dg0t=CNull,Dg1t=CNull, Dg2t=CNull;
	for(int i=0;i<Ng;++i)
	{
		phi=(i+double(0.5))*hG;
		sincos(phi,&SIN, &COS);
		Bessel=j0(double(2.)*z*sin(phi*0.5));
		EXP=exp(-I*a*(phi));
		g0t=g0t+FG0;
		g1t=g1t+FG1;
		g2t=g2t+FG2;
		
		Dg0t=Dg0t+FDG0;
		Dg1t=Dg1t+FDG1;
		Dg2t=Dg2t+FDG2;
		
	}
	g0=hG*g0t;
	g1=hG*g1t;
	g2=hG*g2t;
	Dg0=hG*Dg0t;
	Dg1=hG*Dg1t;
	Dg2=hG*Dg2t;
}




