//анизатропный максвелл
#ifdef __CUDACC__
__host__ __device__
#endif 
inline void distr_AnisotropicMaxwell(double* PartParams,double Norm,double PL,double PT,double *Distrib)
{
			//double m=PartParams[2];//масса
			double velocity=PartParams[5];//скорость направленная
			double theta=PartParams[7];//угол между  velocity и магнитным полем
			double dPl=PartParams[8];//продольный разброс
			double dPt=PartParams[9];//поперечный разброс

			double gamma=(double)1./sqrt((double)1.-velocity*velocity);//гамма фактор
			//считаем импульс обезразмеренным на mc, где m--масса частицы 
			double pt=velocity*sin(theta)*gamma;//поперечный импульс
			double pl=velocity*cos(theta)*gamma;//продольный импульс
			//Distrib[0]=Norm*exp(-(PT-pt)*(PT-pt)/(dPt*dPt)-(PL-pl)*(PL-pl)/(dPl*dPl));
			Distrib[0]=Norm*exp(-(PT-pt)*(PT-pt)/(dPt*dPt)-(PL-pl)*(PL-pl)/(dPl*dPl));
			//производная по продольному импульсу
			//нормировка содержится в  Distrib[0]
			//DlDistrib
			Distrib[1]=(-(double)2.*(PL-pl)/(dPl*dPl))*Distrib[0];
			//производная по поперечному импульсу
			//DtDistrib
			Distrib[2]=(-(double)2.*(PT-pt)/(dPt*dPt))*Distrib[0];
}
	
//аналитическая нормировка
#ifdef __CUDACC__
__host__ __device__
#endif 
inline void distr_Norm_AnisotropicMaxwell(double* PartParams,double &Norm)
{
	//double m=PartParams[2];//масса
	double velocity=PartParams[5];//скорость направленная
	double theta=PartParams[7];//угол между  velocity и магнитным полем
	double dPl=PartParams[8];//продольный разброс
	double dPt=PartParams[9];//поперечный разброс

	double gamma=(double)1./sqrt((double)1.-velocity*velocity);//гамма фактор
	double pt=velocity*sin(theta)*gamma;//поперечный импульс
	double pl=velocity*cos(theta)*gamma;//продольный импульс

	
	Norm=(double)1./(dPl*dPt*dPt*M_PI*sqrt(M_PI)*((pt/dPt)*sqrt(M_PI)*erf(pt/dPt)+sqrt(M_PI)*pt/dPt+exp(-pt*pt/(dPt*dPt))));			
}
