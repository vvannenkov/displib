//анизатропный максвелл
#ifdef __CUDACC__
__host__ __device__
#endif 
inline void distr_Moon(double* PartParams,double Norm,double PL,double PT,double *Distrib)
{

		//double m=PartParams[2];//масса
		double velocity=PartParams[5];//скорость направленная
		
		double dP=PartParams[7];//разброс по импульсам

		double gamma=(double)1./sqrt((double)1.-velocity*velocity);//гамма фактор
		double pB=velocity*gamma;//продольный направленный импульс
		double dtheta=PartParams[8];//угловой разброс в месте создания
		double R=PartParams[9];//пробочное отношение
		double pt = PT/sqrt(R);
		double pl = sqrt(PL*PL - pt*pt + PT*PT);
		double p0 = sqrt(pt*pt + pl*pl);
		//double Norm=PartParams[16];
		double theta =acos(pl/p0);
		
		Distrib[0]=Norm*exp(-(p0-pB)*(p0-pB)/(dP*dP)-theta*theta/(dtheta*dtheta));

		double P0 = sqrt(PT*PT + PL*PL);
		double P1 = sqrt(PL*PL+(PT*PT*(-(double)1.+R))/R);
		double theta1 = acos(P1/P0);
		//производная по продольному импульсу
		//нормировка содержится в  Distrib[0]
		//DlDistrib
		Distrib[1]=Distrib[0]*(((double)2.*PL*((pB-P0)/(dP*dP)+(sqrt(PT*PT/((PL*PL+PT*PT)*R))*theta1)/(dtheta*dtheta*P1)))/P0);

		
		//производная по поперечному импульсу
		//DtDistrib
		Distrib[2]=Distrib[0]*(((double)2.*PT*(-(double)1.+pB/P0))/(dP*dP)-((double)2.*PL*PL*P1*sqrt(PT*PT/((PL*PL+PT*PT)*R))*R*theta1)/(dtheta*dtheta*P0*(PT*PT*PT*(-(double)1.+R)+PL*PL*PT*R)));
		
}
	
