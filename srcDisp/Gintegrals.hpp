// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

//внутренний заголовочный файл

#pragma once

//G-functions
#define FG0  (Bessel-double(1.))*EXP
#define FG1  SIN*Bessel*EXP
#define FG2	 COS*Bessel*EXP

#define FG11 SIN05*Bessel1*EXP


#define FDG0 (Bessel-double(1.))*(-I*gamP*(phi)/Omega)*EXP
#define FDG1 SIN*Bessel*(-I*gamP*(phi)/Omega)*EXP
#define FDG2 COS*Bessel*(-I*gamP*(phi)/Omega)*EXP
