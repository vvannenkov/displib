

#include <complex>

using namespace std;
using namespace std::complex_literals;

#include "ClassEpsilon.hpp"
#include "DispLocal.hpp"



//find a solution of dispersion equation using Newton's method
extern "C" void NewtonsMethod(double w0_real,double w0_imag,double kl, double kt,int NumOfSorts, double** PartParams, double** IntParams, double* w_res,double Precision,int Iter_max)
{

const complex<double> I=complex<double>(0.,1.0), CNull=complex<double>(0.,0.);

double *Disp;//значение дисперсионного уравнения [0-1] и производная [2-3] 
Disp=new double[4];

double *Epsilon;//суммарное значение компонент тензора (сумма всех сортов)
double *TempEpsilon;//значения компонент тензора
Epsilon=new double[36];
TempEpsilon=new double[36];


complex<double> F=CNull,DF=CNull,dw=CNull,w=CNull;
/*
Res[0]=FUNC.real();
Res[1]=FUNC.imag();
Res[2]=DFUNC.real();
Res[3]=DFUNC.imag();
*/
int count=0;
w=complex<double>(w0_real,w0_imag);

w_res[0]=0;
w_res[1]=0;
for (int i=0;i<Iter_max;i++)
{
	VacEps(Epsilon);
	for(int s=0;s<NumOfSorts;s++)
	{	
		Eps(w.real(),w.imag(),kl, kt,PartParams[s],IntParams[s],TempEpsilon);
		EpsSumm(Epsilon,TempEpsilon,Epsilon);
	}
	Dispersion(w.real(),w.imag(),kl, kt, Epsilon,Disp);
	F=complex<double>(Disp[0],Disp[1]);
	DF=complex<double>(Disp[2],Disp[3]);

	dw=F/DF;
	w-= dw;
	if(abs(dw) < Precision) 
	{
		w_res[0]=w.real();
		w_res[1]=w.imag();
		break;
	}//Convergence
}

delete Disp;
delete Epsilon;
delete TempEpsilon;

}


