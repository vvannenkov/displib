// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

//обёртка эпсилона для OpenMP

extern "C" void HotEpsMP(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res);
extern "C" void ColdPlasma(double w_real,double w_imag,double kl, double kt,double* PartParams,double *Res);

extern "C" void Eps(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res)
{
	if(PartParams[0]==0)//cold
		ColdPlasma(w_real,w_imag,kl, kt,PartParams,Res);
	else
		HotEpsMP(w_real,w_imag,kl, kt,PartParams,IntParams,Res);
}

#include <iostream>		// std::cout
using namespace std;
//print properties of all CUDA devices 
extern "C" void AllDevicesProp(void)
{
	
	cout<<"To know GPU properties you need to recompile the library with the CUDA compiler"<<endl;
}
//print properties of one CUDA device
extern "C" void DeviceProp(const int gpu)
{
	cout<<"To know GPU properties you need to recompile the library with the CUDA compiler"<<endl;
}
