// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

//подынтегральные выражения, необхоимые привычислении компонент тензора в рамках кинетической теории


//общий случай
#define FEPSZZ1   PL*(PT*DLDISTRIB/gamma-PL*DTDISTRIB/gamma-PL*U*(a*I*C0*g0-double(1.))/(Omega*a))
	#define FEPSXX1   a*U*I*a*g0*C0
	#define FEPSYY1   PT*PT*U*g2*C0
	#define FEPSXY1   PT*PT*U*g1*C0
	#define FEPSXZ1   PL*U*I*a*g0*C0
	#define FEPSYZ1   PL*PT*PT*U*g1*C0/a



//Derivatives 
	#define FDEPSXX1  PT*PT*(gamma*U*(I*a*g0*C0)/Omega+a*DTDISTRIB*(I*a*g0*C0)+a*U*(I*gamma*g0*C0/Omega+I*a*g0*DC0+I*a*Dg0*C0))
	#define FDEPSYY1   PT*PT*(DTDISTRIB*g2*C0+g2*DC0*U+Dg2*C0*U)
	#define FDEPSZZ1  -PL*(-((PL*U*(-double(1.)+I*a*C0*g0)*(gamma/Omega))/(Omega*a*a))+(PL*U*(I*C0*g0*(gamma/Omega)+I*a*g0*(DC0)+I*a*C0*(Dg0)))/(Omega*a)+(PL*(-double(1.)+I*a*C0*g0)*(DTDISTRIB)/(Omega*a)))
	#define FDEPSXY1   PT*PT*(DTDISTRIB*g1*C0+U*g1*DC0+U*Dg1*C0)
	#define FDEPSXZ1  PL*DTDISTRIB*(I*a*g0*C0)+PL*U*(I*gamma*g0*C0/Omega+I*a*g0*DC0+I*a*Dg0*C0)
	#define FDEPSYZ1  PL*PT*PT*(g1*C0*DTDISTRIB/a+g1*U*DC0/a+Dg1*U*C0/a-g1*gamma*U*C0/(a*a*Omega))
	
//волны с поперечным распространением	
	#define FEPSZZ2   PL*(PT*DLDISTRIB/gamma-I*PL*w/Omega*DTDISTRIB*C0*g0)
	#define FEPSXX2   gamma*gamma*DTDISTRIB*g0*C0
	#define FEPSYY2   PT*PT*DTDISTRIB*g2*C0
	#define FEPSXY2   PT*PT*DTDISTRIB*g1*C0
	#define FEPSXZ2   PL*gamma*DTDISTRIB*g0*C0
	#define FEPSYZ2   PL*PT*PT*DTDISTRIB*g1*C0/gamma

//Derivatives 
	#define FDEPSXX2  gamma*gamma*Dg0*C0*DTDISTRIB+gamma*gamma*g0*DC0*DTDISTRIB
	#define FDEPSYY2  PT*PT*DTDISTRIB*Dg2*C0+PT*PT*DTDISTRIB*g2*DC0
	#define FDEPSZZ2  -PL*(I*PL/(w*Omega)*DTDISTRIB*Dg0*C0+I*PL/(w*Omega)*DTDISTRIB*g0*DC0)
	#define FDEPSXY2  PT*PT*DTDISTRIB*Dg1*C0+PT*PT*DTDISTRIB*g1*DC0
	#define FDEPSXZ2  gamma*PL*DTDISTRIB*Dg0*C0+gamma*PL*DTDISTRIB*g0*DC0
	#define FDEPSYZ2  PL*PT*PT/gamma*DTDISTRIB*Dg1*C0+PL*PT/gamma*DTDISTRIB*g1*DC0
	
//волны с продольным распространением	
	#define FEPSZZ3   PT*PL*DLDISTRIB/a
	#define FEPSXX3   PT*PT*U*a/(a*a-double(1.))
	//#define FEPSYY1   1  та же, что и FEPSXX1
	#define FEPSXY3   PT*PT*U/(a*a-double(1.))
	//#define FEPSXZ1   0
	//#define FEPSYZ1   0


//Derivatives 
	#define FDEPSXX3  PT*PT*DTDISTRIB*a/(a*a-double(1.))-PT*PT*gamma*U*(a*a+double(1.))/(a*a*a*a-double(2.)*a*a+double(1.))/Omega
	//#define FDEPSYY1  та же, что и FDEPSXX1 
	#define FDEPSZZ3  PL*PT*gamma/(a*a)*DLDISTRIB
	#define FDEPSXY3  PT*PT*DTDISTRIB/(a*a-double(1.))-PT*PT*U*double(2.)*a*gamma/(Omega*(a*a-double(1.))*(a*a-double(1.)))
	//#define FDEPSXZ1  1
	//#define FDEPSYZ1  1

//незамагниченная плазма
#define FEPSU1	PT*PT*U/gamma
#define FEPSU2	PT*PT*V/gamma
#define FEPSU3	PT*PT*w*DLDISTRIB/gamma
#define FEPSU4	PT*PL*V/gamma
#define FEPSU5	PT*PL*w*DLDISTRIB/gamma

//Derivatives
#define FDEPSU1	PT*PT*DTDISTRIB/gamma
#define FDEPSU3	PT*PT*DLDISTRIB/gamma
#define FDEPSU5	PT*PL*DLDISTRIB/gamma
