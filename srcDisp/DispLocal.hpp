// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

//внутренний заголовочный файл

#pragma once



extern void EpsToRes(double *Res,epsilon eps);
extern void ResToEps(double *Res,epsilon &eps);

extern "C" void EpsSumm(double *Eps1,double *Eps2,double* Res);
extern "C" void PrintEps(double *Epsilon,int Prec);

extern void  CalcAllGint(double Omega,double gamP,complex<double> a,double z,int Ng,complex<double> &g0,complex<double> &g1, complex<double> &g2,complex<double> &Dg0,complex<double> &Dg1, complex<double> &Dg2);

extern  void Distribution(double* PartParams,double Norm,double PL,double PT,double *Distrib);
extern int Distribution_ParamNum(double* PartParams);

extern "C" void	ColdPlasma(double w_real,double w_imag,double kl, double kt,double* PartParams,double *Res);

extern void CheckIntParams(double *IntParams);
extern void CheckPartParams(double *PartParams);

//вакуумные эпсилоны
extern "C" void VacEps(double *Res);

//главная функция. Возвращает Eps. (горячий или холодный) Считает или на GPU, или на CPU по openMP в зависимости от компиляции библиотеки
extern "C" void Eps(double w_real,double w_imag,double kl, double kt,double* PartParams,double* IntParams,double *Res);
extern "C" void Dispersion(double w_real,double w_imag,double kl, double kt, double* Epsilon, double* Res);
