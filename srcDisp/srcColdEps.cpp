// Copyright © 2021 Vladimir Annenkov. All rights reserved.
// Licensed under the Apache License, Version 2.0

//Холодные эпсилоны

//#include <stdio.h>
//#include <stdlib.h>
//#include <iostream>
#include <complex>

using namespace std;
using namespace std::complex_literals;

#include "ClassEpsilon.hpp"
#include "DispLocal.hpp"


extern "C" void ColdPlasma(double w_real,double w_imag,double kl, double kt,double* PartParams,double *Res){
	
	epsilon eps;
	const complex<double> I=complex<double>(0.,1.0);
	complex<double> w=complex<double>(w_real,w_imag);
	double n=PartParams[1];//концентрация
	double m=PartParams[2];//масса
	double q=PartParams[3];//зарядовое число (со знаком, определеющим знак заряда)
	double Omega=PartParams[4]*q/m;//Циклотронная частота
	double velocity=PartParams[5];//скорость
	double gamma=double(1.)/sqrt(double(1.)-velocity*velocity);
	double Momentum=velocity*gamma;//импульс
	double Coeff1=(q*q/m);
	//исправленный знак из-за заряда

		eps.XX=-(w*gamma-kl*Momentum)*(w*gamma-kl*Momentum)*Coeff1/(((w*gamma-kl*Momentum)*(w*gamma-kl*Momentum)-Omega*Omega)*w*w*gamma);
		eps.XY=I*(w*gamma-kl*Momentum)*Omega*Coeff1/(((w*gamma-kl*Momentum)*(w*gamma-kl*Momentum)-Omega*Omega)*w*w*gamma);
		eps.XZ=-kt*Momentum*(w*gamma-kl*Momentum)*Coeff1/(((w*gamma-kl*Momentum)*(w*gamma-kl*Momentum)-Omega*Omega)*w*w*gamma);

		//проверить минус тут, если что
		eps.YZ=-I*kt*Momentum*Omega*Coeff1/(((w*gamma-kl*Momentum)*(w*gamma-kl*Momentum)-Omega*Omega)*w*w*gamma);
		eps.ZZ=-Coeff1/((w*gamma-kl*Momentum)*(w*gamma-kl*Momentum)*gamma)-Coeff1*kt*kt*Momentum*Momentum/(gamma*w*w*((w*gamma-kl*Momentum)*(w*gamma-kl*Momentum)-Omega*Omega));

		eps.YY=eps.XX;
		eps.YX=-eps.XY;
		eps.ZX=eps.XZ;
		eps.ZY=-eps.YZ;

//производные
		eps.XXD=(double(2.)*Coeff1*((-kl)*Momentum + gamma*w)*((-kl*kl*kl)*Momentum*Momentum*Momentum+double(3.)*gamma*kl*kl*Momentum*Momentum*w + gamma*gamma*gamma*w*w*w + kl*Momentum*(Omega*Omega - double(3.)*gamma*gamma*w*w)))/(gamma*w*w*w*(Omega*Omega- (kl*Momentum - gamma*w)*(kl*Momentum - gamma*w))*(Omega*Omega- (kl*Momentum - gamma*w)*(kl*Momentum - gamma*w)));

		eps.XYD=-((I*Coeff1*Omega*(-double(2.)*kl*kl*kl*Momentum*Momentum*Momentum - gamma*Omega*Omega*w + double(7.)*gamma*kl*kl*Momentum*Momentum*w + double(3.)*gamma*gamma*gamma*w*w*w + double(2.)*kl*Momentum*(Omega*Omega - double(4.)*gamma*gamma*w*w)))/(gamma*w*w*w*(Omega*Omega- (kl*Momentum - gamma*w)*(kl*Momentum - gamma*w))*(Omega*Omega- (kl*Momentum - gamma*w)*(kl*Momentum - gamma*w))));

		eps.XZD=-(Coeff1*kt*Momentum*(double(2.)*kl*kl*kl*Momentum*Momentum*Momentum + gamma*Omega*Omega*w - double(7.)*gamma*kl*kl*Momentum*Momentum*w - double(3.)*gamma*gamma*gamma*w*w*w - double(2.)*kl*Momentum*(Omega*Omega - double(4.)*gamma*gamma*w*w)))/(gamma*w*w*w*(Omega*Omega- (kl*Momentum - gamma*w)*(kl*Momentum - gamma*w))*(Omega*Omega- (kl*Momentum - gamma*w)*(kl*Momentum - gamma*w)));

		eps.YZD=-((double(2.)*I*Coeff1*kt*Omega*Momentum*(Omega*Omega - (kl*Momentum - double(2.)*gamma*w)*(kl*Momentum - gamma*w)))/(gamma*w*w*w*(Omega*Omega- (kl*Momentum - gamma*w)*(kl*Momentum - gamma*w))*(Omega*Omega- (kl*Momentum - gamma*w)*(kl*Momentum - gamma*w))));

		eps.ZZD=double(2.)*Coeff1*(double(1.)/(((-kl)*Momentum + gamma*w)*((-kl)*Momentum + gamma*w)*((-kl)*Momentum + gamma*w)) + (kt*kt*Momentum*Momentum*(-Omega*Omega + (kl*Momentum - double(2.)*gamma*w)*(kl*Momentum - gamma*w)))/(gamma*w*w*w*(Omega + kl*Momentum - gamma*w)*(Omega + kl*Momentum - gamma*w)*(Omega - kl*Momentum + gamma*w)*(Omega - kl*Momentum + gamma*w)));

		eps.YYD=eps.XXD;
		eps.YXD=-eps.XYD;
		eps.ZXD=eps.XZD;
		eps.ZYD=-eps.YZD;

EpsToRes(Res,n*eps);		
}





